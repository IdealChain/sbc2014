﻿using Apache.NMS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.ActiveMQ
{
    public class EventLogger : IDisposable
    {
        public static string CONST_STORAGE_EVENTLOG = "STORAGE_EVENTLOG";

        private ISession logMqSession;
        private IQueue storageEventLog;
        private IMessageProducer storageEventProducer = null;

        public EventLogger(ISession logMqSession)
        {
            this.logMqSession = logMqSession;
            storageEventLog = logMqSession.GetQueue(CONST_STORAGE_EVENTLOG);
            storageEventProducer = logMqSession.CreateProducer(storageEventLog);
        }

        public void sendEventLog(EventLogType type, Object obj, String collection)
        {
            EventLogEntry entry = new EventLogEntry(type, obj, collection);
            IObjectMessage msg = logMqSession.CreateObjectMessage(entry);
            storageEventProducer.Send(msg);
        }

        public void Dispose()
        {
            if (storageEventProducer != null)
                storageEventProducer.Close();
            if (logMqSession != null)
                logMqSession.Close();
            storageEventLog = null;
        }
    }
}
