﻿using System.Threading;
using Apache.NMS;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;
using Feuerwerksfabrik.Common.Objects;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.ActiveMQ
{
    public class ActiveMqEngine : IFeuerwerksfabrikEngine, IDisposable
    {
        public static readonly TimeSpan CONST_TIMESPAN_SHORT = new TimeSpan(0, 0, 0, 0, 300);
        public static readonly TimeSpan CONST_TIMESPAN_LONG = new TimeSpan(0, 0, 0, 1, 0);

        public static string CONST_AUFTRAEGE_STORAGE = "AUFTRAEGE_STORAGE";
        public static string CONST_FINISHED_AUFTRAEGE_STORAGE = "FINISHED_AUFTRAEGE_STORAGE";
        public static string CONST_USED_TREIBLADUNGEN_STORAGE = "USED_TREIBLADUNGEN_STORAGE";
        public static string CONST_FINISHED_ROCKET_STORAGE = "FINISHED_ROCKET_STORAGE";
        public static string CONST_VERIFIED_ROCKET_ORDERS_STORAGE = "VERIFIED_ROCKET_ORDERS_STORAGE";
        public static string CONST_VERIFIED_ROCKET_CLASSA_STORAGE = "VERIFIED_ROCKET_CLASSA_STORAGE";
        public static string CONST_VERIFIED_ROCKET_CLASSB_STORAGE = "VERIFIED_ROCKET_CLASSB_STORAGE";
        public static string CONST_VERIFIED_ROCKET_DEFECT_STORAGE = "VERIFIED_ROCKET_DEFECT_STORAGE";
        public static string CONST_DEFECT_ROCKET_STORAGE = "DEFECT_ROCKET_STORAGE";
        public static string CONST_EFFEKTLADUNG_BLUE_STORAGE = "EFFEKTLADUNG_BLUE_STORAGE";
        public static string CONST_EFFEKTLADUNG_RED_STORAGE = "EFFEKTLADUNG_RED_STORAGE";
        public static string CONST_EFFEKTLADUNG_GREEN_STORAGE = "EFFEKTLADUNG_GREEN_STORAGE";

        private bool disposing = false;
        private ISession activeMqSession;
        private EventLogger eventLogger;

        private IQueue storageEventLog;
        private IMessageConsumer storageEventConsumer = null;

        public ActiveMqEngine(ISession activeMqSession)
        {
            this.activeMqSession = activeMqSession;
            eventLogger = new EventLogger(activeMqSession);

            Auftraege = new ObservableCollection<Auftrag>();
            AvailableParts = new ObservableCollection<Bestandteil>();
            FinishedRockets = new ObservableCollection<Rakete>();
            VerifiedRockets = new ObservableCollection<Rakete>();
            DeliveredRocketPackages = new ObservableCollection<Raketenpackung>();
            DefectRockets = new ObservableCollection<Rakete>();

            storageEventLog = activeMqSession.GetQueue(EventLogger.CONST_STORAGE_EVENTLOG);
            storageEventConsumer = activeMqSession.CreateConsumer(storageEventLog);
            storageEventConsumer.Listener += new MessageListener(onStorageEventLogMessage);

            //DEBUG:
            /*IQueue auftraegeStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_AUFTRAEGE_STORAGE);
            IMessageProducer auftraegeProducer = activeMqSession.CreateProducer(auftraegeStorage);
            Auftrag auftrag = Auftrag.Create("herbert", "tcp://localhost:39922/", CONST_DEFECT_ROCKET_STORAGE, 1, Effektfarbe.Rot, Effektfarbe.Rot, Effektfarbe.Rot);
            IMessage msg = activeMqSession.CreateObjectMessage(auftrag);
            auftraegeProducer.Send(msg);
            eventLogger.sendEventLog(EventLogType.ADD, auftrag, CONST_AUFTRAEGE_STORAGE);
            activeMqSession.Commit();*/
        }

        public void onStorageEventLogMessage(IMessage msg)
        {
            IObjectMessage iom = (IObjectMessage)msg;
            EventLogEntry entry = (EventLogEntry)iom.Body;
            if (entry.collection.Equals(CONST_AUFTRAEGE_STORAGE))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (Auftraege)
                        Auftraege.Add((Auftrag)entry.obj);
                else
                    lock (Auftraege)
                        Auftraege.Remove((Auftrag)entry.obj);
            else if (entry.collection.Equals(CONST_FINISHED_AUFTRAEGE_STORAGE))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (Auftraege)
                        Auftraege.Add((Auftrag)entry.obj);
                else
                    lock (Auftraege)
                        Auftraege.Remove((Auftrag)entry.obj);
            else if (entry.collection.Equals(typeof(Holzstab).Name))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (AvailableParts)
                        AvailableParts.Add((Holzstab)entry.obj);
                else
                    lock (AvailableParts) 
                        AvailableParts.Remove((Holzstab)entry.obj);
            else if (entry.collection.Equals(typeof(Gehaeuse).Name))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (AvailableParts) 
                        AvailableParts.Add((Gehaeuse)entry.obj);
                else
                    lock (AvailableParts) 
                        AvailableParts.Remove((Gehaeuse)entry.obj);
            else if (entry.collection.Equals(typeof(Treibladungspackung).Name))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (AvailableParts) 
                        AvailableParts.Add((Treibladungspackung)entry.obj);
                else
                    lock (AvailableParts) 
                        AvailableParts.Remove((Treibladungspackung)entry.obj);
            else if (entry.collection.Equals(CONST_USED_TREIBLADUNGEN_STORAGE))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (AvailableParts)
                        AvailableParts.Add((Treibladungspackung)entry.obj);
                else
                    lock (AvailableParts)
                        AvailableParts.Remove((Treibladungspackung)entry.obj);
            else if (entry.collection.Equals(CONST_EFFEKTLADUNG_BLUE_STORAGE))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (AvailableParts) 
                        AvailableParts.Add((Effektladung)entry.obj);
                else
                    lock (AvailableParts) 
                        AvailableParts.Remove((Effektladung)entry.obj);
            else if (entry.collection.Equals(CONST_EFFEKTLADUNG_RED_STORAGE))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (AvailableParts)
                        AvailableParts.Add((Effektladung)entry.obj);
                else
                    lock (AvailableParts)
                        AvailableParts.Remove((Effektladung)entry.obj);
            else if (entry.collection.Equals(CONST_EFFEKTLADUNG_GREEN_STORAGE))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (AvailableParts)
                        AvailableParts.Add((Effektladung)entry.obj);
                else
                    lock (AvailableParts)
                        AvailableParts.Remove((Effektladung)entry.obj);
            else if (entry.collection.Equals(CONST_FINISHED_ROCKET_STORAGE))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (FinishedRockets)
                        FinishedRockets.Add((Rakete)entry.obj);
                else
                    lock (FinishedRockets)
                        FinishedRockets.Remove((Rakete)entry.obj);
            else if (entry.collection.Equals(CONST_VERIFIED_ROCKET_ORDERS_STORAGE))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (VerifiedRockets)
                        VerifiedRockets.Add((Rakete)entry.obj);
                else
                    lock (VerifiedRockets)
                        VerifiedRockets.Remove((Rakete)entry.obj);
            else if (entry.collection.Equals(CONST_VERIFIED_ROCKET_CLASSA_STORAGE))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (VerifiedRockets)
                        VerifiedRockets.Add((Rakete)entry.obj);
                else
                    lock (VerifiedRockets)
                        VerifiedRockets.Remove((Rakete)entry.obj);
            else if (entry.collection.Equals(CONST_VERIFIED_ROCKET_CLASSB_STORAGE))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (VerifiedRockets)
                        VerifiedRockets.Add((Rakete)entry.obj);
                else
                    lock (VerifiedRockets)
                        VerifiedRockets.Remove((Rakete)entry.obj);
            else if (entry.collection.Equals(CONST_VERIFIED_ROCKET_DEFECT_STORAGE))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (VerifiedRockets)
                        VerifiedRockets.Add((Rakete)entry.obj);
                else
                    lock (VerifiedRockets)
                        VerifiedRockets.Remove((Rakete)entry.obj);
            else if (entry.collection.Equals(CONST_DEFECT_ROCKET_STORAGE))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (DefectRockets)
                        DefectRockets.Add((Rakete)entry.obj);
                else
                    lock (DefectRockets)
                        DefectRockets.Remove((Rakete)entry.obj);
            else if (entry.collection.Equals(typeof(Raketenpackung).Name))
                if (entry.type.Equals(EventLogType.ADD))
                    lock (DeliveredRocketPackages)
                        DeliveredRocketPackages.Add((Raketenpackung)entry.obj);
                else
                    lock (DeliveredRocketPackages)
                        DeliveredRocketPackages.Remove((Raketenpackung)entry.obj);
        }

        public Task CreateLieferantTask(Lieferant lieferant, CancellationToken token)
        {
            return new Task(() =>
            {
                ISession taskActiveMqSession = ActiveMqFactory.CreateSession();
                IMessageProducer producer;
                if (lieferant.BestandteilType == typeof(Effektladung))
                {
                    if (lieferant.EffectColor.Equals(Effektfarbe.Blau))
                    {
                        producer = taskActiveMqSession.CreateProducer(taskActiveMqSession.GetQueue(CONST_EFFEKTLADUNG_BLUE_STORAGE));
                    }
                    else if (lieferant.EffectColor.Equals(Effektfarbe.Rot))
                    {
                        producer = taskActiveMqSession.CreateProducer(taskActiveMqSession.GetQueue(CONST_EFFEKTLADUNG_RED_STORAGE));
                    }
                    else // if (lieferant.EffectColor.Equals(Effektfarbe.Gruen))
                    {
                        producer = taskActiveMqSession.CreateProducer(taskActiveMqSession.GetQueue(CONST_EFFEKTLADUNG_GREEN_STORAGE));
                    }
                }
                else
                {
                    producer = taskActiveMqSession.CreateProducer(taskActiveMqSession.GetQueue(lieferant.BestandteilType.Name));
                }
                EventLogger taskEventLogger = new EventLogger(taskActiveMqSession);

                for (int i = 0; i < lieferant.Count && !token.IsCancellationRequested; i++)
                {
                    Bestandteil part = Bestandteil.Create(lieferant);
                    Logger.Log("{0} delivering {1}", lieferant.ID, part);
                    Helpers.SleepRandom(1000, 2000);
                    IMessage msg = producer.CreateObjectMessage(part);
                    producer.Send(msg);
                    taskEventLogger.sendEventLog(EventLogType.ADD, part, lieferant.BestandteilType.Name);
                    taskActiveMqSession.Commit();
                }
                Logger.Log("{0} {1}", lieferant.ID, token.IsCancellationRequested ? "canceled" : "done");
            });
        }

        public void Dispose()
        {
            disposing = true;

            if (storageEventConsumer != null)
            {
                storageEventConsumer.Close();
                storageEventConsumer = null;
            }
            if (eventLogger != null)
            {
                eventLogger.Dispose();
                eventLogger = null;
            }
            if (activeMqSession != null)
            {
                activeMqSession.Close();
                activeMqSession = null;
            }
        }

        public ObservableCollection<Auftrag> Auftraege { get; private set; }

        public ObservableCollection<Bestandteil> AvailableParts { get; private set; }

        public ObservableCollection<Rakete> FinishedRockets { get; private set; }

        public ObservableCollection<Rakete> VerifiedRockets { get; private set; }

        public ObservableCollection<Raketenpackung> DeliveredRocketPackages { get; private set; }

        public ObservableCollection<Rakete> DefectRockets { get; private set; }
    }
}
