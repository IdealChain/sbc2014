﻿using Apache.NMS;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;
using Feuerwerksfabrik.Common.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.ActiveMQ.Tasks
{
    class QaWorker : IDisposable
    {
        public static QaWorker Create(QA qa, ISession activeMqSession)
        {
            QaWorker qaWorker = new QaWorker();
            qaWorker.qa = qa;
            qaWorker.activeMqSession = activeMqSession;
            qaWorker.eventLogger = new EventLogger(activeMqSession);
            return qaWorker;
        }

        private IQueue auftraegeStorage;
        private IMessageConsumer auftraegeConsumer;
        private IMessageProducer auftraegeProducer;
        private IQueue finishedAuftraegeStorage;
        private IMessageProducer finishedAuftraegeProducer;
        private IQueue finishedRocketsStorage;
        private IMessageConsumer finishedRocketsConsumer;
        private IQueue verifiedRocketsOrderStorage;
        private IMessageProducer verifiedRocketsOrderProducer;
        private IQueue verifiedRocketsClassAStorage;
        private IMessageProducer verifiedRocketsClassAProducer;
        private IQueue verifiedRocketsClassBStorage;
        private IMessageProducer verifiedRocketsClassBProducer;
        private IQueue verifiedRocketsDefectStorage;
        private IMessageProducer verifiedRocketsDefectProducer;

        private QA qa;
        private ISession activeMqSession;
        private EventLogger eventLogger;
        private bool disposing = false;

        public void Run()
        {
            Logger.Log("{0}: Starting", qa.ID);
            auftraegeStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_AUFTRAEGE_STORAGE);
            auftraegeConsumer = activeMqSession.CreateConsumer(auftraegeStorage);
            auftraegeProducer = activeMqSession.CreateProducer(auftraegeStorage);
            finishedAuftraegeStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_FINISHED_AUFTRAEGE_STORAGE);
            finishedAuftraegeProducer = activeMqSession.CreateProducer(finishedAuftraegeStorage);
            finishedRocketsStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_FINISHED_ROCKET_STORAGE);
            finishedRocketsConsumer = activeMqSession.CreateConsumer(finishedRocketsStorage);
            verifiedRocketsOrderStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_VERIFIED_ROCKET_ORDERS_STORAGE);
            verifiedRocketsOrderProducer = activeMqSession.CreateProducer(verifiedRocketsOrderStorage);
            verifiedRocketsClassAStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_VERIFIED_ROCKET_CLASSA_STORAGE);
            verifiedRocketsClassAProducer = activeMqSession.CreateProducer(verifiedRocketsClassAStorage);
            verifiedRocketsClassBStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_VERIFIED_ROCKET_CLASSB_STORAGE);
            verifiedRocketsClassBProducer = activeMqSession.CreateProducer(verifiedRocketsClassBStorage);
            verifiedRocketsDefectStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_VERIFIED_ROCKET_DEFECT_STORAGE);
            verifiedRocketsDefectProducer = activeMqSession.CreateProducer(verifiedRocketsDefectStorage);

            try
            {
                while (!disposing)
                {
                    IObjectMessage iom = (IObjectMessage)finishedRocketsConsumer.Receive();
                    Rakete currentRocket = (Rakete)iom.Body;
                    eventLogger.sendEventLog(EventLogType.REMOVE, currentRocket, ActiveMqEngine.CONST_FINISHED_ROCKET_STORAGE);

                    currentRocket.DetermineQuality();
                    currentRocket.Mitarbeiter.Add(qa);
                    if (currentRocket.Auftrag != null)
                    {
                        if (currentRocket.Qualitaet == Raketenqualitaet.Defekt || currentRocket.Qualitaet == Raketenqualitaet.KlasseB)
                        {
                            tellAuftragOneMoreToBuild(currentRocket.Auftrag);
                        }
                        else if (currentRocket.Qualitaet == Raketenqualitaet.KlasseA)
                        {
                            tellAuftragOneQualityOk(currentRocket.Auftrag);
                        }
                    }

                    Logger.Log("{0}: Checked rocket {1}: {2}", qa.ID, currentRocket.ID, currentRocket.Qualitaet.ToString());
                    if (currentRocket.Auftrag != null && currentRocket.Qualitaet == Raketenqualitaet.KlasseA)
                    {
                        IObjectMessage msg = verifiedRocketsOrderProducer.CreateObjectMessage(currentRocket);
                        verifiedRocketsOrderProducer.Send(msg);
                        eventLogger.sendEventLog(EventLogType.ADD, currentRocket, ActiveMqEngine.CONST_VERIFIED_ROCKET_ORDERS_STORAGE);
                    }
                    else if (currentRocket.Auftrag == null && currentRocket.Qualitaet == Raketenqualitaet.KlasseA)
                    {
                        IObjectMessage msg = verifiedRocketsClassAProducer.CreateObjectMessage(currentRocket);
                        verifiedRocketsClassAProducer.Send(msg);
                        eventLogger.sendEventLog(EventLogType.ADD, currentRocket, ActiveMqEngine.CONST_VERIFIED_ROCKET_CLASSA_STORAGE);
                    }
                    else if (currentRocket.Qualitaet == Raketenqualitaet.KlasseB)
                    {
                        IObjectMessage msg = verifiedRocketsClassBProducer.CreateObjectMessage(currentRocket);
                        verifiedRocketsClassBProducer.Send(msg);
                        eventLogger.sendEventLog(EventLogType.ADD, currentRocket, ActiveMqEngine.CONST_VERIFIED_ROCKET_CLASSB_STORAGE);
                    }
                    else if (currentRocket.Qualitaet == Raketenqualitaet.Defekt)
                    {
                        IObjectMessage msg = verifiedRocketsDefectProducer.CreateObjectMessage(currentRocket);
                        verifiedRocketsDefectProducer.Send(msg);
                        eventLogger.sendEventLog(EventLogType.ADD, currentRocket, ActiveMqEngine.CONST_VERIFIED_ROCKET_DEFECT_STORAGE);
                    }

                    activeMqSession.Commit();
                }
            }
            finally
            {
                activeMqSession.Rollback();
                finishedRocketsConsumer.Close();
                verifiedRocketsOrderProducer.Close();
                verifiedRocketsClassAProducer.Close();
                verifiedRocketsClassBProducer.Close();
                verifiedRocketsDefectProducer.Close();
                Dispose();
            }
        }

        private void tellAuftragOneMoreToBuild(Auftrag auftrag)
        {
            bool found = false;
            while (!found)
            {
                IObjectMessage iom = (IObjectMessage)auftraegeConsumer.Receive();
                Auftrag cauftrag = (Auftrag)iom.Body;
                if (auftrag.Equals(cauftrag))
                {
                    eventLogger.sendEventLog(EventLogType.REMOVE, cauftrag, ActiveMqEngine.CONST_AUFTRAEGE_STORAGE);
                    cauftrag.anzahlBereitsGebaut--;
                    found = true;
                    eventLogger.sendEventLog(EventLogType.ADD, cauftrag, ActiveMqEngine.CONST_AUFTRAEGE_STORAGE);
                }
                IObjectMessage msg = activeMqSession.CreateObjectMessage(cauftrag);
                auftraegeProducer.Send(msg);
            }
        }

        private void tellAuftragOneQualityOk(Auftrag auftrag)
        {
            bool found = false;
            while (!found)
            {
                IObjectMessage iom = (IObjectMessage)auftraegeConsumer.Receive();
                Auftrag cauftrag = (Auftrag)iom.Body;
                if (auftrag.Equals(cauftrag))
                {
                    eventLogger.sendEventLog(EventLogType.REMOVE, cauftrag, ActiveMqEngine.CONST_AUFTRAEGE_STORAGE);
                    cauftrag.anzahlBereitsUeberprueft++;
                    found = true;
                }

                if (cauftrag.anzahlBereitsUeberprueft >= cauftrag.anzahl)
                {
                    cauftrag.Status = AuftragsStatus.Erledigt;
                    IObjectMessage msg = activeMqSession.CreateObjectMessage(cauftrag);
                    finishedAuftraegeProducer.Send(msg);
                    eventLogger.sendEventLog(EventLogType.ADD, cauftrag, ActiveMqEngine.CONST_FINISHED_AUFTRAEGE_STORAGE);
                }
                else
                {
                    IObjectMessage msg = activeMqSession.CreateObjectMessage(cauftrag);
                    auftraegeProducer.Send(msg);
                    eventLogger.sendEventLog(EventLogType.ADD, cauftrag, ActiveMqEngine.CONST_AUFTRAEGE_STORAGE);
                }
            }
        }

        public void Dispose()
        {
            if (!disposing)
            {
                disposing = true;
                Logger.Log("Disposing, stopping QaWorker {0}", qa.ID);
                qa = null;
                if (eventLogger != null)
                {
                    eventLogger.Dispose();
                    eventLogger = null;
                }
                if (activeMqSession != null)
                {
                    activeMqSession.Close();
                    activeMqSession = null;
                }
            }
        }
    }
}
