﻿using Apache.NMS;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;
using Feuerwerksfabrik.Common.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.ActiveMQ.Tasks
{
    class ProduzentWorker : IDisposable
    {
        private bool disposing = false;
        private Produzent produzent;
        private Auftrag auftrag;
        private ISession activeMqSession;
        private EventLogger eventLogger;
        private Random Random;

        private IQueue auftraegeStorage;
        private IMessageConsumer auftraegeConsumer;
        private IMessageProducer auftraegeProducer;
        private IQueue holzstabStorage;
        private IMessageConsumer holzstabConsumer;
        private IQueue gehaeuseStorage;
        private IMessageConsumer gehaeuseConsumer;
        private IQueue TreibladungspackungStorage;
        private IMessageConsumer TreibladungspackungConsumer;
        private IQueue UsedTreibladungspackungStorage;
        private IMessageConsumer UsedTreibladungspackungConsumer;
        private IMessageProducer UsedTreibladungspackungProducer;
        private IQueue EffektladungBlueStorage;
        private IQueue EffektladungRedStorage;
        private IQueue EffektladungGreenStorage;
        private IMessageConsumer EffektladungBlueConsumer;
        private IMessageConsumer EffektladungRedConsumer;
        private IMessageConsumer EffektladungGreenConsumer;
        private IQueue finishedRocketsStorage;
        private IMessageProducer finishedRocketsProducer;

        public static ProduzentWorker Create(Produzent produzent, ISession activeMqSession)
        {
            return new ProduzentWorker(produzent, activeMqSession);
        }

        private ProduzentWorker(Produzent produzent, ISession activeMqSession)
        {
            this.produzent = produzent;
            this.activeMqSession = activeMqSession;

            eventLogger = new EventLogger(activeMqSession);
            Random = new Random();

            auftraegeStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_AUFTRAEGE_STORAGE);
            auftraegeConsumer = activeMqSession.CreateConsumer(auftraegeStorage);
            auftraegeProducer = activeMqSession.CreateProducer(auftraegeStorage);
            holzstabStorage = activeMqSession.GetQueue(typeof(Holzstab).Name);
            holzstabConsumer = activeMqSession.CreateConsumer(holzstabStorage);
            gehaeuseStorage = activeMqSession.GetQueue(typeof(Gehaeuse).Name);
            gehaeuseConsumer = activeMqSession.CreateConsumer(gehaeuseStorage);
            TreibladungspackungStorage = activeMqSession.GetQueue(typeof(Treibladungspackung).Name);
            TreibladungspackungConsumer = activeMqSession.CreateConsumer(TreibladungspackungStorage);
            UsedTreibladungspackungStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_USED_TREIBLADUNGEN_STORAGE);
            UsedTreibladungspackungConsumer = activeMqSession.CreateConsumer(UsedTreibladungspackungStorage);
            UsedTreibladungspackungProducer = activeMqSession.CreateProducer(TreibladungspackungStorage);
            EffektladungBlueStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_EFFEKTLADUNG_BLUE_STORAGE);
            EffektladungRedStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_EFFEKTLADUNG_RED_STORAGE);
            EffektladungGreenStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_EFFEKTLADUNG_GREEN_STORAGE);
            EffektladungBlueConsumer = activeMqSession.CreateConsumer(EffektladungBlueStorage);
            EffektladungRedConsumer = activeMqSession.CreateConsumer(EffektladungRedStorage);
            EffektladungGreenConsumer = activeMqSession.CreateConsumer(EffektladungGreenStorage);

            finishedRocketsStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_FINISHED_ROCKET_STORAGE);
            finishedRocketsProducer = activeMqSession.CreateProducer(finishedRocketsStorage);
        }

        public void Run()
        {
            while (!disposing)
                BuildNewRocket();
        }

        public void BuildNewRocket()
        {
            Logger.Log("{0}: Begin to build a new rocket", produzent.ID);

            // new rocket
            var rocket = Rakete.Create(produzent);

            try
            {
                Auftrag tauftrag = takeAuftragFromStorage();
                if (tauftrag != null)
                {
                    if (tauftrag.anzahlBereitsGebaut < tauftrag.anzahl)
                    {
                        tauftrag.anzahlBereitsGebaut++;
                        auftrag = tauftrag;
                        rocket.Auftrag = tauftrag;
                    }
                    else
                    {
                        auftrag = null;
                    }
                    IObjectMessage msg = activeMqSession.CreateObjectMessage(tauftrag);
                    auftraegeProducer.Send(msg);
                    eventLogger.sendEventLog(EventLogType.ADD, tauftrag, ActiveMqEngine.CONST_AUFTRAEGE_STORAGE);
                }

                var holzstabTask = Task.Run((Func<Holzstab>)takeHolzstabFromStorage);
                var gehauseTask = Task.Run((Func<Gehaeuse>)takeGehaeuseFromStorage);
                var treibladungsTask = Task.Run((Func<List<Treibladungspackung>>)takeTreibladungspackungenFromStorage);
                var effektladungsTask = Task.Run((Func<List<Effektladung>>)takeEffektLadungenFromStorage);

                try
                {
                    Task.WaitAll(holzstabTask, gehauseTask, treibladungsTask, effektladungsTask);
                }
                catch (DisposingException)
                {
                    //if the tasks were canceled from a shutdown while waiting for items:
                    return;
                }

                Logger.Log("{0}: all parts ready to assemble", produzent.ID);
                Helpers.SleepRandom(1000, 2000);

                rocket.Bestandteile.Add(holzstabTask.Result);
                rocket.Bestandteile.Add(gehauseTask.Result);
                rocket.Bestandteile.UnionWith(treibladungsTask.Result);
                rocket.Bestandteile.UnionWith(effektladungsTask.Result);

                Logger.Log("{0}: assembled rocket {1}, adding it to finishedRocketsStorage", produzent.ID, rocket.ID);
                IObjectMessage rocketMsg = finishedRocketsProducer.CreateObjectMessage(rocket);
                finishedRocketsProducer.Send(rocketMsg);
                eventLogger.sendEventLog(EventLogType.ADD, rocket, ActiveMqEngine.CONST_FINISHED_ROCKET_STORAGE);

                activeMqSession.Commit();
            }
            catch (AggregateException e)
            {
                activeMqSession.Rollback();
                // ignore all task exceptions when disposing
                e.Handle(x => disposing);
            }
        }

        private List<Treibladungspackung> takeTreibladungspackungenFromStorage()
        {
            var packungen = new List<Treibladungspackung>();
            var noetigeMenge = Random.Next(115, 145); //130g  +/-15g
            var menge = 0;

            while (menge < noetigeMenge)
            {
                var packung = takeTreibladungspackungFromStorage();
                var entnahme = packung.Take(noetigeMenge - menge);

                menge += entnahme.Treibladung;
                packungen.Add(entnahme);

                if (packung.Stand != PackungsStand.Leer)
                {
                    Logger.Log("{0}: Returning Treibladungspackung ({1}) to Available Parts", produzent.ID, packung.ToString());
                    IMessage msg = UsedTreibladungspackungProducer.CreateObjectMessage(packung);
                    UsedTreibladungspackungProducer.Send(msg);
                    eventLogger.sendEventLog(EventLogType.ADD, packung, typeof(Treibladungspackung).Name);
                    //.Put(null, packung, new LabelSelector<string>("Stand", packung.Stand.ToString()));
                }
            }

            return packungen;
        }

        private Treibladungspackung takeTreibladungspackungFromStorage()
        {
            Treibladungspackung packung;
            // prefer already used packages
            IObjectMessage iom = (IObjectMessage)UsedTreibladungspackungConsumer.Receive(new TimeSpan(0, 0, 1));

            if (iom != null)
            {
                packung = (Treibladungspackung)iom.Body;
                eventLogger.sendEventLog(EventLogType.REMOVE, packung, typeof(Treibladungspackung).Name);
            }
            else
            {
                // if there is no used Treibladungspackung in the Storage, we try to take a new one:
                iom = (IObjectMessage)TreibladungspackungConsumer.Receive();
                packung = (Treibladungspackung)iom.Body;
                eventLogger.sendEventLog(EventLogType.REMOVE, packung, typeof(Treibladungspackung).Name);
            }
            Logger.Log("{0}: collected Treibladungspackung {1}", produzent.ID, packung.ID);
            return packung;
        }

        private List<Effektladung> takeEffektLadungenFromStorage()
        {
            var ladungen = new List<Effektladung>();

            int farbNr = 0;
            while (ladungen.Count < Rakete.CONST_EffektLadungenNeededPerRocket)
            {
                IObjectMessage iom;
                if (auftrag != null)
                {
                    if (auftrag.farben[farbNr].Equals(Effektfarbe.Blau))
                        iom = (IObjectMessage)EffektladungBlueConsumer.Receive();
                    else if (auftrag.farben[farbNr].Equals(Effektfarbe.Rot))
                        iom = (IObjectMessage)EffektladungRedConsumer.Receive();
                    else // if (auftrag.farben[farbNr].Equals(Effektfarbe.Gruen))
                        iom = (IObjectMessage)EffektladungGreenConsumer.Receive();
                }
                else
                {
                    iom = null;
                    while (iom == null)
                    {
                        iom = (IObjectMessage)EffektladungBlueConsumer.Receive(ActiveMqEngine.CONST_TIMESPAN_SHORT);
                        if (iom != null)
                            break;
                        iom = (IObjectMessage)EffektladungRedConsumer.Receive(ActiveMqEngine.CONST_TIMESPAN_SHORT);
                        if (iom != null)
                            break;
                        iom = (IObjectMessage)EffektladungGreenConsumer.Receive(ActiveMqEngine.CONST_TIMESPAN_SHORT);
                        if (iom != null)
                            break;
                    }
                }
                var effektladung = (Effektladung)iom.Body;
                eventLogger.sendEventLog(EventLogType.REMOVE, effektladung, typeof(Effektladung).Name);
                Logger.Log("{0}: collected Effektladung {1}", produzent.ID, effektladung.ID);
                ladungen.Add(effektladung);
                farbNr++;
            }
            return ladungen;
        }

        private Auftrag takeAuftragFromStorage()
        {
            IObjectMessage iom;
            iom = (IObjectMessage)auftraegeConsumer.Receive(ActiveMqEngine.CONST_TIMESPAN_SHORT);
            if (iom == null)
            {
                //no orders to process:
                return null;
            }
            var auftrag = (Auftrag)iom.Body;
            Logger.Log("{0}: collected Auftrag {1}", produzent.ID, auftrag.ID);
            eventLogger.sendEventLog(EventLogType.REMOVE, auftrag, ActiveMqEngine.CONST_AUFTRAEGE_STORAGE);
            return auftrag;
        }

        private Holzstab takeHolzstabFromStorage()
        {
            IObjectMessage iom = (IObjectMessage)holzstabConsumer.Receive();
            if (iom == null)
            {
                throw new DisposingException();
            }
            var holzstab = (Holzstab)iom.Body;
            Logger.Log("{0}: collected Holzstab {1}", produzent.ID, holzstab.ID);
            eventLogger.sendEventLog(EventLogType.REMOVE, holzstab, typeof(Holzstab).Name);
            return holzstab;
        }

        private Gehaeuse takeGehaeuseFromStorage()
        {
            IObjectMessage iom = (IObjectMessage)gehaeuseConsumer.Receive();
            if (iom == null)
            {
                throw new DisposingException();
            }
            var gehaeuse = (Gehaeuse)iom.Body;
            Logger.Log("{0}: collected Gehaeuse {1}", produzent.ID, gehaeuse.ID);
            eventLogger.sendEventLog(EventLogType.REMOVE, gehaeuse, typeof(Gehaeuse).Name);
            return gehaeuse;
        }

        public void Dispose()
        {
            disposing = true;
            if (holzstabConsumer != null)
            {
                holzstabConsumer.Close();
                holzstabConsumer = null;
            }
            if (finishedRocketsProducer != null)
            {
                finishedRocketsProducer.Close();
                finishedRocketsProducer = null;
            }
            if (eventLogger != null)
            {
                eventLogger.Dispose();
                eventLogger = null;
            }
            if (activeMqSession != null)
            {
                activeMqSession.Close();
                activeMqSession = null;
            }
        }
    }
}
