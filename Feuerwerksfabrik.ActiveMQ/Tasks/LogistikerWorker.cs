﻿using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;
using Feuerwerksfabrik.Common.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.ActiveMQ.Tasks
{
    public class LogistikerWorker : IDisposable
    {
        private Logistiker logistiker;
        private bool disposing = false;
        private ISession activeMqSession;
        private EventLogger eventLogger;

        private IQueue finishedAuftraegeStorage;
        private IMessageConsumer finishedAuftraegeConsumer;
        private IMessageProducer finishedAuftraegeProducer;
        private IQueue verifiedRocketsOrderStorage;
        private IMessageProducer verifiedRocketsOrderProducer;
        private IMessageConsumer verifiedRocketsOrderConsumer;
        private IQueue verifiedRocketsClassAStorage;
        private IMessageConsumer verifiedRocketsClassAConsumer;
        private IQueue verifiedRocketsClassBStorage;
        private IMessageConsumer verifiedRocketsClassBConsumer;
        private IQueue verifiedRocketsDefectStorage;
        private IMessageConsumer verifiedRocketsDefectConsumer;
        private IQueue defectRocketsStorage;
        private IMessageProducer defectRocketsProducer;
        private IQueue packagedRocketsStorage;
        private IMessageProducer packagedRocketsProducer;

        public static LogistikerWorker Create(Logistiker logistiker, ISession activeMqSession)
        {
            var lWorker = new LogistikerWorker();
            lWorker.logistiker = logistiker;
            lWorker.activeMqSession = activeMqSession;
            lWorker.eventLogger = new EventLogger(activeMqSession);
            return lWorker;
        }

        public void Run()
        {
            Logger.Log("{0}: Starting", logistiker.ID);

            finishedAuftraegeStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_FINISHED_AUFTRAEGE_STORAGE);
            finishedAuftraegeConsumer = activeMqSession.CreateConsumer(finishedAuftraegeStorage);
            finishedAuftraegeProducer = activeMqSession.CreateProducer(finishedAuftraegeStorage);
            verifiedRocketsOrderStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_VERIFIED_ROCKET_ORDERS_STORAGE);
            verifiedRocketsOrderProducer = activeMqSession.CreateProducer(verifiedRocketsOrderStorage);
            verifiedRocketsOrderConsumer = activeMqSession.CreateConsumer(verifiedRocketsOrderStorage);
            verifiedRocketsClassAStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_VERIFIED_ROCKET_CLASSA_STORAGE);
            verifiedRocketsClassAConsumer = activeMqSession.CreateConsumer(verifiedRocketsClassAStorage);
            verifiedRocketsClassBStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_VERIFIED_ROCKET_CLASSB_STORAGE);
            verifiedRocketsClassBConsumer = activeMqSession.CreateConsumer(verifiedRocketsClassBStorage);
            verifiedRocketsDefectStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_VERIFIED_ROCKET_DEFECT_STORAGE);
            verifiedRocketsDefectConsumer = activeMqSession.CreateConsumer(verifiedRocketsDefectStorage);
            defectRocketsStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_DEFECT_ROCKET_STORAGE);
            defectRocketsProducer = activeMqSession.CreateProducer(defectRocketsStorage);
            packagedRocketsStorage = activeMqSession.GetQueue(ActiveMqEngine.CONST_DEFECT_ROCKET_STORAGE);
            packagedRocketsProducer = activeMqSession.CreateProducer(packagedRocketsStorage);

            try
            {
                while (!disposing)
                {
                    Raketenpackung packung = Raketenpackung.Create(logistiker);

                    // trashing defect rockets:
                    while (true)
                    {
                        IObjectMessage iomDefect = (IObjectMessage)verifiedRocketsDefectConsumer.Receive(ActiveMqEngine.CONST_TIMESPAN_SHORT);
                        if (iomDefect == null)
                            break;
                        var verifiedRocketDefect = (Rakete)iomDefect.Body;
                        eventLogger.sendEventLog(EventLogType.REMOVE, verifiedRocketDefect, ActiveMqEngine.CONST_VERIFIED_ROCKET_DEFECT_STORAGE);
                        verifiedRocketDefect.Mitarbeiter.Add(logistiker);
                        Logger.Log("{0}: Trashing {1}", logistiker.ID, verifiedRocketDefect.ID);
                        IObjectMessage msg = defectRocketsProducer.CreateObjectMessage(verifiedRocketDefect);
                        defectRocketsProducer.Send(msg);
                        eventLogger.sendEventLog(EventLogType.ADD, verifiedRocketDefect, ActiveMqEngine.CONST_DEFECT_ROCKET_STORAGE);
                        activeMqSession.Commit();
                    }

                    // check for finished Order:
                    IObjectMessage iomAuftrag = (IObjectMessage)finishedAuftraegeConsumer.Receive(ActiveMqEngine.CONST_TIMESPAN_SHORT);
                    if (iomAuftrag != null)
                    {
                        Auftrag auftrag = (Auftrag)iomAuftrag.Body;
                        int alreadyDeliveredRockets = 0;
                        Logger.Log("{0}: Finished Order {1} in Queue, processing:", logistiker.ID, auftrag.ID);

                        List<Rakete> deliveryRockets = new List<Rakete>();
                        while (alreadyDeliveredRockets < auftrag.anzahl)
                        {
                            Rakete verifiedRocketOrder = null;
                            while (verifiedRocketOrder == null)
                            {
                                IObjectMessage iomOrderMsg = (IObjectMessage)verifiedRocketsOrderConsumer.Receive();
                                verifiedRocketOrder = (Rakete)iomOrderMsg.Body;
                                if (!auftrag.Equals(verifiedRocketOrder.Auftrag))
                                {
                                    IObjectMessage returnToQueueMsg = defectRocketsProducer.CreateObjectMessage(verifiedRocketOrder);
                                    verifiedRocketsOrderProducer.Send(returnToQueueMsg);
                                    verifiedRocketOrder = null;
                                }
                            }
                            eventLogger.sendEventLog(EventLogType.REMOVE, verifiedRocketOrder, ActiveMqEngine.CONST_VERIFIED_ROCKET_ORDERS_STORAGE);
                            verifiedRocketOrder.Mitarbeiter.Add(logistiker);
                            deliveryRockets.Add(verifiedRocketOrder);
                            alreadyDeliveredRockets++;
                        }

                        try
                        {
                            ConnectionFactory factory = new ConnectionFactory(auftrag.deliverAddress);
                            IConnection conn = factory.CreateConnection();
                            conn.Start();
                            ISession remoteSession = conn.CreateSession(AcknowledgementMode.Transactional);
                            IQueue deliverStorage = remoteSession.GetQueue(auftrag.deliverStorage);
                            IMessageProducer deliverStorageProducer = remoteSession.CreateProducer(deliverStorage);
                            Logger.Log("{0}: Connection to Deliver-Address ready: address:{1}, storage:{2}", logistiker.ID, auftrag.deliverAddress, auftrag.deliverStorage);

                            foreach(Rakete rakete in deliveryRockets) {
                                Logger.Log("{0}: delivering {1}", logistiker.ID, rakete.ID);
                                IObjectMessage msg = deliverStorageProducer.CreateObjectMessage(rakete);
                                deliverStorageProducer.Send(msg);
                                //eventLogger.sendEventLog(EventLogType.ADD, verifiedRocketOrder, ActiveMqEngine.CONST_DEFECT_ROCKET_STORAGE);
                            }

                            activeMqSession.Commit();
                            remoteSession.Commit();
                            //clean up remote connection:
                            remoteSession.Close();
                            conn.Close();
                            conn.Dispose();
                            conn = null;
                            factory = null;
                        }
                        catch (Apache.NMS.NMSConnectionException ex)
                        {
                            //customer is offline, can't transfer delivery
                            //storing delivery in local depot:
                            IQueue deliveryDepot = activeMqSession.GetQueue("depot_" + auftrag.auftraggeber);
                            IMessageProducer deliveryDepotProducer = activeMqSession.CreateProducer(deliveryDepot);

                            foreach (Rakete rakete in deliveryRockets)
                            {
                                Logger.Log("{0}: storing {1} in depot {2}", logistiker.ID, rakete.ID, auftrag.auftraggeber);
                                IObjectMessage msg = deliveryDepotProducer.CreateObjectMessage(rakete);
                                deliveryDepotProducer.Send(msg);
                                //eventLogger.sendEventLog(EventLogType.ADD, verifiedRocketOrder, ActiveMqEngine.CONST_DEFECT_ROCKET_STORAGE);
                            }
                            activeMqSession.Commit();
                        }
                    }

                    // the real logistic work:
                    IObjectMessage iom = null;
                    Rakete verifiedRocket = null;
                    string rocketSourceStorage = null;
                    IMessageConsumer rocketSource = null;
                    while (iom == null)
                    {
                        iom = (IObjectMessage)verifiedRocketsClassAConsumer.Receive(ActiveMqEngine.CONST_TIMESPAN_SHORT);
                        if (iom != null)
                        {
                            verifiedRocket = (Rakete)iom.Body;
                            eventLogger.sendEventLog(EventLogType.REMOVE, verifiedRocket, ActiveMqEngine.CONST_VERIFIED_ROCKET_CLASSA_STORAGE);
                            rocketSourceStorage = ActiveMqEngine.CONST_VERIFIED_ROCKET_CLASSA_STORAGE;
                            rocketSource = verifiedRocketsClassAConsumer;
                            break;
                        }
                        iom = (IObjectMessage)verifiedRocketsClassBConsumer.Receive(ActiveMqEngine.CONST_TIMESPAN_SHORT);
                        if (iom != null)
                        {
                            verifiedRocket = (Rakete)iom.Body;
                            eventLogger.sendEventLog(EventLogType.REMOVE, verifiedRocket, ActiveMqEngine.CONST_VERIFIED_ROCKET_CLASSB_STORAGE);
                            rocketSourceStorage = ActiveMqEngine.CONST_VERIFIED_ROCKET_CLASSB_STORAGE;
                            rocketSource = verifiedRocketsClassBConsumer;
                            break;
                        }
                    }

                    verifiedRocket.Mitarbeiter.Add(logistiker);
                    if (packung.Qualitaet == Raketenqualitaet.Offen)
                    {
                        packung.Qualitaet = verifiedRocket.Qualitaet;
                    }
                    Logger.Log("{0}: Adding {1} to {2}", logistiker.ID, verifiedRocket.ID, packung.ID);
                    packung.Raketen.Add(verifiedRocket);

                    while (packung.Stand != PackungsStand.Voll)
                    {
                        iom = (IObjectMessage)rocketSource.Receive();
                        verifiedRocket = (Rakete)iom.Body;
                        eventLogger.sendEventLog(EventLogType.REMOVE, verifiedRocket, rocketSourceStorage);
                        verifiedRocket.Mitarbeiter.Add(logistiker);
                        Logger.Log("{0}: Adding {1} to {2}", logistiker.ID, verifiedRocket.ID, packung.ID);
                        packung.Raketen.Add(verifiedRocket);
                    }



                    /*IObjectMessage iom = (IObjectMessage)verifiedRocketsOrderConsumer.Receive();
                    var verifiedRocket = (Rakete)iom.Body;

                    if (verifiedRocket.Qualitaet == Raketenqualitaet.Defekt)
                    {
                        verifiedRocket.Mitarbeiter.Add(logistiker);
                        Logger.Log("{0}: Trashing {1}", logistiker.ID, verifiedRocket.ID);
                        IObjectMessage msg = defectRocketsProducer.CreateObjectMessage(verifiedRocket);
                        defectRocketsProducer.Send(msg);
                        eventLogger.sendEventLog(EventLogType.ADD, verifiedRocket, ActiveMqEngine.CONST_DEFECT_ROCKET_STORAGE);
                        continue;
                    }

                    if (packung.Qualitaet == Raketenqualitaet.Offen)
                    {
                        packung.Qualitaet = verifiedRocket.Qualitaet;
                    }
                    else
                    {
                        if (packung.Qualitaet != verifiedRocket.Qualitaet)
                        {
                            verifiedRocketsProducer.Send(iom);
                            continue;
                        }
                    }
                    verifiedRocket.Mitarbeiter.Add(logistiker);
                    Logger.Log("{0}: Adding {1} to {2}", logistiker.ID, verifiedRocket.ID, packung.ID);
                    packung.Raketen.Add(verifiedRocket);
                    eventLogger.sendEventLog(EventLogType.REMOVE, verifiedRocket, ActiveMqEngine.CONST_VERIFIED_ROCKET_STORAGE);
                    }*/

                    Logger.Log("{0}: Finished {1}", logistiker.ID, packung.ID);
                    IObjectMessage msg2 = packagedRocketsProducer.CreateObjectMessage(packung);
                    packagedRocketsProducer.Send(msg2);
                    eventLogger.sendEventLog(EventLogType.ADD, packung, typeof(Raketenpackung).Name);

                    activeMqSession.Commit();
                }
            }
            finally
            {
                activeMqSession.Rollback();
                verifiedRocketsOrderConsumer.Close();
                verifiedRocketsClassAConsumer.Close();
                verifiedRocketsClassBConsumer.Close();
                verifiedRocketsDefectConsumer.Close();
                defectRocketsProducer.Close();
                packagedRocketsProducer.Close();
            }

            Dispose();
        }

        public void Dispose()
        {
            if (!disposing)
            {
                disposing = true;
                Logger.Log("Disposing, stopping QaWorker {0}", logistiker.ID);
                logistiker = null;
            }

        }
    }
}