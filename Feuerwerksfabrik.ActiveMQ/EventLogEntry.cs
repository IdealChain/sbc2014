﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.ActiveMQ
{
    [Serializable]
    public class EventLogEntry
    {
        public EventLogEntry(EventLogType type, Object obj, String collection)
        {
            this.type = type;
            this.obj = obj;
            this.collection = collection;
        }

        public EventLogType type { get; private set; }
        public Object obj { get; private set; }
        public String collection { get; private set; }
    }

    public enum EventLogType
    {
        ADD,
        REMOVE
    }
}
