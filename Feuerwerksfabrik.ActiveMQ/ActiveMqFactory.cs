﻿using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Feuerwerksfabrik.ActiveMQ.Tasks;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.ActiveMQ
{
    [Export(typeof(IFeuerwerksfabrikFactory))]
    public class ActiveMqFactory : IFeuerwerksfabrikFactory
    {
        //private static Process activeMqServerProcess;
        private static ConnectionFactory activeMqConnFactory;
        private static IConnection activeMqConnection;

        static ActiveMqFactory()
        {
            //activeMqServerProcess = Process.Start(Settings.Default.ActiveMqServerRunnablePath, Settings.Default.ActiveMqServerRunnableArguments);
            //the ActiveMQ-Server needs some time to start up properly:
            //Thread.Sleep(Settings.Default.ActiveMqRunnableStartTimeBuffer);

            activeMqConnFactory = new ConnectionFactory("tcp://" + Settings.Default.ActiveMqHost + ":" + Settings.Default.ActiveMqPort);
        }

        public static ISession CreateSession()
        {
            if (activeMqConnection == null)
            {
                Logger.Log("Connecting to {0}", activeMqConnFactory.BrokerUri);
                activeMqConnection = activeMqConnFactory.CreateConnection();
                activeMqConnection.Start();
            }

            return activeMqConnection.CreateSession(AcknowledgementMode.Transactional);
        }

        public string Name
        {
            get { return "ActiveMQ"; }
        }

        public IFeuerwerksfabrikEngine Create()
        {
            var session = CreateSession();
            return new ActiveMqEngine(session);
        }

        public Task CreateMitarbeiterTask(Mitarbeiter mitarbeiter)
        {
            var session = CreateSession();

            if (mitarbeiter is Produzent)
            {
                Produzent produzent = (Produzent)mitarbeiter;
                ProduzentWorker pw = ProduzentWorker.Create(produzent, session);
                return new Task(pw.Run);
            }
            else if (mitarbeiter is QA)
            {
                QA qa = (QA)mitarbeiter;
                QaWorker pw = QaWorker.Create(qa, session);
                return new Task(pw.Run);
            }
            else if (mitarbeiter is Logistiker)
            {
                Logistiker logistiker = (Logistiker)mitarbeiter;
                LogistikerWorker pw = LogistikerWorker.Create(logistiker, session);
                return new Task(pw.Run);
            }

            throw new NotSupportedException("Mitarbeiter type not supported: " + mitarbeiter.GetType().Name);
        }

        public static void Dispose()
        {
            if (activeMqConnection != null)
                activeMqConnection.Close();
            activeMqConnFactory = null;
            //stop ActiveMQ-Server:
            //activeMqServerProcess.CloseMainWindow();
        }
    }
}
