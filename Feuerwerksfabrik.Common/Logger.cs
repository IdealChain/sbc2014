﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.Common
{
    public static class Logger
    {
        public static Action<String> LogAction;
        private static readonly ConcurrentDictionary<Assembly, String> assemblyNameCache = new ConcurrentDictionary<Assembly, string>();

        static Logger()
        {
            LogAction += s => Trace.WriteLine(s);
        }

        public static void Log(String msg)
        {
            Log(msg, Assembly.GetCallingAssembly());
        }

        private static String GetAssemblyName(Assembly assembly)
        {
            if (assemblyNameCache.ContainsKey(assembly))
                return assemblyNameCache[assembly];

            var name = assembly.GetName().Name;
            return (assemblyNameCache[assembly] = name);
        }

        public static void Log(String msg, Assembly assembly)
        {
            msg = String.Format("{0}: {1}", GetAssemblyName(assembly), msg);

            try
            {
                if (LogAction != null)
                    LogAction.Invoke(msg);
            }
            catch (Exception e)
            {
                Debug.WriteLine("LogAction: " + e);
            }
        }

        public static void Log(string format, params object[] args)
        {
            var assembly = Assembly.GetCallingAssembly();
            Log(String.Format(format, args), assembly);
        }
    }
}
