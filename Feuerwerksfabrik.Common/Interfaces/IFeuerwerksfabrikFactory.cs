﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feuerwerksfabrik.Common.Actors;

namespace Feuerwerksfabrik.Common.Interfaces
{
    /// <summary>
    /// Factory interface for creating new fireworks engines
    /// </summary>
    public interface IFeuerwerksfabrikFactory
    {
        String Name { get; }

        IFeuerwerksfabrikEngine Create();

        Task CreateMitarbeiterTask(Mitarbeiter data);
    }
}
