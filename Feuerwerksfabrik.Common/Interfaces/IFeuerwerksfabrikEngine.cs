﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Objects;

namespace Feuerwerksfabrik.Common.Interfaces
{
    /// <summary>
    /// Main interface for the GUI to display current state
    /// </summary>
    public interface IFeuerwerksfabrikEngine : IDisposable
    {
        Task CreateLieferantTask(Lieferant data, CancellationToken token);

        ObservableCollection<Auftrag> Auftraege { get; }

        ObservableCollection<Bestandteil> AvailableParts { get; }

        ObservableCollection<Rakete> FinishedRockets { get; }

        ObservableCollection<Rakete> VerifiedRockets { get; }

        ObservableCollection<Raketenpackung> DeliveredRocketPackages { get; }

        ObservableCollection<Rakete> DefectRockets { get; }
    }
}
