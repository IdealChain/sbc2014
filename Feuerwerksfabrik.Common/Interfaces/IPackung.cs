﻿namespace Feuerwerksfabrik.Common.Interfaces
{
    public interface IPackung
    {
        PackungsStand Stand { get; }
    }

    public enum PackungsStand
    {
        Voll,
        Angebrochen,
        Leer
    }
}