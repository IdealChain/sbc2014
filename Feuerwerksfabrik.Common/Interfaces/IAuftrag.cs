﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.Common.Interfaces
{
    interface IAuftrag
    {
        AuftragsStatus Status { get; }
    }

    public enum AuftragsStatus
    {
        Offen,
        Erledigt,
        Ausgeliefert,
        Aufbewahrt
    }
}
