﻿namespace Feuerwerksfabrik.Common.Interfaces
{
    public interface IIdentifiable
    {
        ID ID { get; set; }
    }
}
