﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.Common
{
    /// <summary>
    /// Identifier class, automatically generating a new ID number if not supplied.
    /// Can be implicitely casted/converted from and to <see cref="String"/>.
    /// </summary>
    [Serializable]
    public class ID
    {
        private string Identifier { get; set; }

        public ID(String identifier)
        {
            Identifier = identifier;
        }

        public ID(String type, int number)
        {
            Identifier = String.Format("{0:0000}", number);

            if (!String.IsNullOrEmpty(type))
                Identifier = String.Format("{0}-{1}", type.ToLowerInvariant(), Identifier);
        }

        public static implicit operator String(ID id)
        {
            return id.Identifier;
        }

        public static implicit operator ID(String id)
        {
            return new ID(id);
        }

        private static readonly Dictionary<String, int> NextID = new Dictionary<String, int>();

        public static ID GenerateID(String type)
        {
            lock (NextID)
            {
                if (!NextID.ContainsKey(type))
                    NextID[type] = 0;
                return new ID(type, NextID[type]++);
            }
        }

        public static ID GenerateID(Type type)
        {
            return GenerateID(type.Name);
        }

        public static ID GenerateID<T>()
        {
            return GenerateID(typeof(T));
        }

        public override string ToString()
        {
            return Identifier;
        }

        public override bool Equals(object obj)
        {
            var other = obj as ID;
            if (other == null)
                return false;

            return other.ToString().Equals(ToString());
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }
    }
}
