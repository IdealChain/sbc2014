﻿using Feuerwerksfabrik.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.Common.Objects
{
    [Serializable]
    public class Auftrag : IEquatable<Auftrag>, IAuftrag
    {
        public ID ID { get; protected set; }
        public int anzahl { get; protected set; }
        public int anzahlBereitsGebaut { get; set; }
        public int anzahlBereitsUeberprueft { get; set; }
        public List<Effektfarbe> farben { get; protected set; }
        public string auftraggeber { get; protected set; }
        public string deliverAddress { get; protected set; }
        public string deliverStorage { get; protected set; }
        public bool delivered { get; set; }

        public static Auftrag Create(ID auftraggeber, string deliverAddress, string deliverStorage, int anzahl, Effektfarbe farbe1, Effektfarbe farbe2, Effektfarbe farbe3)
        {
            Auftrag auftrag = new Auftrag();
            auftrag.ID = ID.GenerateID(typeof(Auftrag).Name + "(" + auftraggeber + ")");
            auftrag.auftraggeber = auftraggeber;
            auftrag.anzahl = anzahl;
            auftrag.deliverAddress = deliverAddress;
            auftrag.deliverStorage = deliverStorage;
            auftrag.anzahlBereitsGebaut = 0;
            auftrag.anzahlBereitsUeberprueft = 0;
            auftrag.farben = new List<Effektfarbe> { farbe1, farbe2, farbe3 };
            return auftrag;
        }

        public String AnzahlToString
        {
            get { return String.Format("{0}/{1}/{2}", anzahlBereitsGebaut, anzahlBereitsUeberprueft, anzahl); }
        }

        public String LieferungToString
        {
            get { return String.Format("{0}/{1}", deliverAddress, deliverStorage); }
        }

        public String FarbenToString
        {
            get { return String.Join(", ", farben); }
        }

        public override string ToString()
        {
            return ID.ToString() + "(\n"
                + "Anzahl=" + AnzahlToString
                + ",\nAuftraggeber=" + auftraggeber
                + ",\nFarben=" + FarbenToString + ")\n";
        }

        public bool Equals(Auftrag other)
        {
            if (ID.Equals(other.ID))
                return true;
            else
                return false;
        }

        public AuftragsStatus Status
        {
            get;
            set;
        }
    }
}
