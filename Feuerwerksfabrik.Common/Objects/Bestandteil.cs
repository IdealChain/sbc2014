﻿using System;
using System.Reflection;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;

namespace Feuerwerksfabrik.Common.Objects
{
    [Serializable]
    public abstract class Bestandteil : IEquatable<Bestandteil>
    {
        public ID ID { get; protected set; }

        public ID Lieferant { get; protected set; }

        public static Bestandteil Create(Lieferant lieferant)
        {
            var part = Activator.CreateInstance(lieferant.BestandteilType) as Bestandteil;
            if (part == null)
                throw new Exception("Bestandteil type required");

            part.ID = ID.GenerateID(lieferant.BestandteilType);
            part.Lieferant = lieferant.ID;

            var effektLadung = part as Effektladung;
            if (effektLadung != null)
            {
                effektLadung.FailureRate = lieferant.FailureRate;
                effektLadung.Farbe = lieferant.EffectColor.GetValueOrDefault();
            }

            return part;
        }

        public override string ToString()
        {
            return ID.ToString();
        }

        public bool Equals(Bestandteil other)
        {
            if (ID.Equals(other.ID))
                return true;
            else
                return false;
        }
    }
}
