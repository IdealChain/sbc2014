﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;

namespace Feuerwerksfabrik.Common.Objects
{
    [Serializable]
    public class Rakete : IIdentifiable, IEquatable<Rakete>
    {
        public static readonly int CONST_EffektLadungenNeededPerRocket = 3;

        public Rakete()
        {
            Bestandteile = new HashSet<Bestandteil>();
            Mitarbeiter = new HashSet<Mitarbeiter>();
            Qualitaet = Raketenqualitaet.Offen;
        }

        public static Rakete Create(Produzent produzent)
        {
            Rakete rakete = new Rakete();
            rakete.ID = ID.GenerateID(typeof(Rakete).Name + "(" + produzent.ID + ")");
            rakete.Mitarbeiter.Add(produzent);
            return rakete;
        }

        public ID ID { get; set; }

        public int Treibladung
        {
            get { return Bestandteile.OfType<Treibladungspackung>().Sum(p => p.Treibladung); }
        }

        public Raketenqualitaet Qualitaet { get; set; }

        public Auftrag Auftrag { get; set; }

        public ISet<Bestandteil> Bestandteile { get; private set; }

        public ISet<Mitarbeiter> Mitarbeiter { get; private set; }

        public override string ToString()
        {
            string value = ID.ToString();
            if (Qualitaet != Raketenqualitaet.Offen)
            {
                value += "[" + Qualitaet + "] ";
            }
            if (Bestandteile.Count > 0)
            {
                value += " (Treibladung=" + Treibladung + "g,\n Auftrag: " + Auftrag + ",\n Bestandteile: {\n";
                value = Bestandteile.Aggregate(value, (current, teil) => current + (teil + ",\n"));
                value += "}, Mitarbeiter: {";
                value = Mitarbeiter.Aggregate(value, (current, mitarbeiter) => current + (mitarbeiter.ID + ",\n"));
                value += "})";
            }
            return value;
        }

        /// <summary>
        /// Determines/updates the quality class of this rocket
        /// </summary>
        public void DetermineQuality()
        {
            var workingEffects = Bestandteile.OfType<Effektladung>().Count(e => !e.Defekt);

            if (Treibladung >= 130 && workingEffects >= 3)
                Qualitaet = Raketenqualitaet.KlasseA;
            else if (Treibladung >= 120 && workingEffects >= 2)
                Qualitaet = Raketenqualitaet.KlasseB;
            else
                Qualitaet = Raketenqualitaet.Defekt;
        }

        public bool Equals(Rakete other)
        {
            return ID.Equals(other.ID);
        }
    }

    public enum Raketenqualitaet
    {
        Offen,
        KlasseA,
        KlasseB,
        Defekt
    }
}
