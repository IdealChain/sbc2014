﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feuerwerksfabrik.Common.Interfaces;

namespace Feuerwerksfabrik.Common.Objects
{
    [Serializable]
    public class Treibladungspackung : Bestandteil, IPackung
    {
        private const int Menge = 500;

        public Treibladungspackung()
        {
            Treibladung = Menge;
        }

        public int Treibladung { get; set; }

        public PackungsStand Stand
        {
            get
            {
                if (Treibladung >= Menge)
                    return PackungsStand.Voll;
                if (Treibladung > 0)
                    return PackungsStand.Angebrochen;
                return PackungsStand.Leer;
            }
        }

        public override string ToString()
        {
            return base.ToString() + " (" + Treibladung + "g)";
        }

        public Treibladungspackung Take(int menge)
        {
            if (menge > Treibladung)
                menge = Treibladung;

            Treibladung -= menge;
            return new Treibladungspackung
            {
                ID = ID,
                Lieferant = Lieferant,
                Treibladung = menge
            };
        }
    }
}
