﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.Common.Objects
{
    [Serializable]
    public class Effektladung : Bestandteil
    {
        private static readonly Random Random = new Random();

        public bool Defekt { get; private set; }

        public Effektfarbe Farbe { get; internal set; }

        internal double FailureRate
        {
            set
            {
                Defekt = Random.NextDouble() < value;
            }
        }

        public override string ToString()
        {
            return String.Format("{0} [{1}/{2}]", base.ToString(), Farbe, Defekt ? "Defekt" : "OK");
        }
    }

    public enum Effektfarbe
    {
        Rot,
        Gruen,
        Blau
    }
}
