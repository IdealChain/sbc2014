﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;

namespace Feuerwerksfabrik.Common.Objects
{
    [Serializable]
    public class Raketenpackung : IIdentifiable, IPackung, IEquatable<Raketenpackung>
    {
        private const int Menge = 5;

        public Raketenpackung()
        {
            Raketen = new HashSet<Rakete>();
            Qualitaet = Raketenqualitaet.Offen;
        }

        public static Raketenpackung Create(Logistiker logistiker)
        {
            return new Raketenpackung { ID = ID.GenerateID(typeof(Raketenpackung).Name + "(" + logistiker.ID + ")") };
        }

        public ID ID { get; set; }

        public ISet<Rakete> Raketen { get; private set; }

        public Raketenqualitaet Qualitaet { get; set; }

        public PackungsStand Stand
        {
            get
            {
                if (Raketen.Count >= Menge)
                    return PackungsStand.Voll;
                if (Raketen.Count > 0)
                    return PackungsStand.Angebrochen;
                return PackungsStand.Leer;
            }
        }

        public override string ToString()
        {
            return String.Format("{0} ({1}/{2}) ({{\n{3}\n}})",
                ID,
                Raketen.Count,
                Menge,
                String.Join("\n", Raketen));
        }

        public bool Equals(Raketenpackung other)
        {
            if (ID.Equals(other.ID))
                return true;
            else
                return false;
        }
    }
}
