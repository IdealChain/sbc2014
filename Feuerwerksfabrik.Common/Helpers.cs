﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.Common
{
    public static class Helpers
    {
        private static readonly Random Random = new Random();

        /// <summary>
        /// Disables the <see cref="SleepRandom"/> function.
        /// </summary>
        public static bool SleepRandomDisabled { get; set; }

        /// <summary>
        /// Suspends the current thread for a random time.
        /// </summary>
        /// <param name="min">Minimal duration, milliseconds.</param>
        /// <param name="max">Maximal duration, milliseconds.</param>
        public static void SleepRandom(int min, int max)
        {
            if (SleepRandomDisabled)
                return;

            var timeout = min + Convert.ToInt32(Random.NextDouble() * (max - min));
            Thread.Sleep(timeout);
        }
    }
}
