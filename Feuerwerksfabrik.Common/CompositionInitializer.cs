﻿using System;
using System.ComponentModel.Composition;
using System.ComponentModel.Composition.Hosting;
using System.ComponentModel.Composition.Primitives;
using System.IO;
using System.Linq;
using System.Reflection;

namespace Feuerwerksfabrik.Common
{
    /// <summary>
    /// MEF compositioning helper, catalogs all .dll assemblies by default.
    /// </summary>
    public static class CompositionInitializer
    {
        private static readonly AggregateCatalog Catalog;
        private static readonly CompositionContainer Container;

        static CompositionInitializer()
        {
            Catalog = new AggregateCatalog();
            Container = new CompositionContainer(Catalog, CompositionOptions.DisableSilentRejection);

            var assembly = Assembly.GetEntryAssembly();
            Catalog.Catalogs.Add(new DirectoryCatalog(Path.GetDirectoryName(assembly.Location) ?? ".", "*.dll"));
        }

        /// <summary>
        /// Adds a MEF part catalog for composition.
        /// </summary>
        /// <param name="catalogs"></param>
        public static void AddCatalog(params ComposablePartCatalog[] catalogs)
        {
            foreach (var c in catalogs)
                Catalog.Catalogs.Add(c);
        }

        /// <summary>
        /// Composes the specified parts, i.e. injects all MEF imports.
        /// </summary>
        public static void SatisfyImports(params object[] parts)
        {
            Logger.Log("Composing " + string.Join(", ", parts.Select(p => p.GetType().Name)));
            Container.ComposeParts(parts);
        }

        public static void Dispose()
        {
            if (Container != null)
                Container.Dispose();
        }
    }
}
