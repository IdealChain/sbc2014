﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.Common.Actors
{
    [Serializable]
    public class Produzent : Mitarbeiter
    {
        public static Produzent Create()
        {
            return new Produzent { ID = ID.GenerateID<Produzent>() };
        }
    }
}
