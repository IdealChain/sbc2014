﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.Common.Actors
{
    [Serializable]
    public class QA : Mitarbeiter
    {
        public static QA Create()
        {
            return new QA { ID = Common.ID.GenerateID<QA>() };
        }
    }
}
