﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feuerwerksfabrik.Common.Interfaces;

namespace Feuerwerksfabrik.Common.Actors
{
    [Serializable]
    public abstract class Mitarbeiter : IIdentifiable
    {
        public ID ID { get; set; }

        public override string ToString()
        {
            return ID.ToString();
        }
    }
}
