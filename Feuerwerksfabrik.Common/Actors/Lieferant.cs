﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feuerwerksfabrik.Common.Interfaces;
using Feuerwerksfabrik.Common.Objects;

namespace Feuerwerksfabrik.Common.Actors
{
    [Serializable]
    public class Lieferant : Mitarbeiter
    {
        private Lieferant()
        { }

        public Lieferant(Type bestandteil, int count, double failureRate = 0, Effektfarbe? effectColor = null)
        {
            ID = ID.GenerateID<Lieferant>();
            BestandteilType = bestandteil;
            Count = count;
            FailureRate = failureRate;
            EffectColor = effectColor;
        }

        public Type BestandteilType { get; private set; }
        public int Count { get; set; }
        public double FailureRate { get; set; }
        public Effektfarbe? EffectColor { get; set; }
    }
}
