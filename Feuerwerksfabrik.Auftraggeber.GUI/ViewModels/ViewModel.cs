﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Data;
using System.Windows.Input;
using Feuerwerksfabrik.Auftraggeber;
using Feuerwerksfabrik.Auftraggeber.ActiveMQ;
using Feuerwerksfabrik.Auftraggeber.GUI.Annotations;
using Feuerwerksfabrik.Auftraggeber.Space;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Objects;
using Feuerwerksfabrik.GUI.Helpers;

namespace Feuerwerksfabrik.Auftraggeber.GUI.ViewModels
{
    public sealed class ViewModel : INotifyPropertyChanged, IDisposable
    {
        private readonly Effektfarbe[] selectedColors = { Effektfarbe.Rot, Effektfarbe.Gruen, Effektfarbe.Blau };

        private ICommand orderCommand;
        private IAuftraggeberFactory selectedEngine;
        private IAuftraggeberEngine activeEngine;
        private string deliverySpace = String.Empty;
        private string deliveryStorage = String.Empty;
        private string auftraggeber = String.Empty;

        public ViewModel()
        {
            ID = String.Format("auftraggeber-{0}", Process.GetCurrentProcess().Id);
            RocketCount = 10;
        }

        public ID ID { get; private set; }

        public IEnumerable<IAuftraggeberFactory> Engines
        {
            get { return new List<IAuftraggeberFactory> { new SpaceAuftraggeberFactory(), new ActiveMqAuftraggeberFactory() }; }
        }

        public IAuftraggeberFactory SelectedEngine
        {
            get { return selectedEngine; }
            set
            {
                if (selectedEngine == value)
                    return;

                selectedEngine = value;

                if (value != null)
                {
                    Auftraggeber = "einauftraggeber";
                    DeliverySpace = value.DefaultDeliverySpace;
                    DeliveryStorage = value.DefaultDeliveryStorage;
                }

                OnPropertyChanged();
                CreateEngine();
            }
        }

        private void ClearEngine()
        {
            if (ActiveEngine != null)
            {
                ActiveEngine.Dispose();
                ActiveEngine = null;
            }
        }

        public void CreateEngine()
        {
            ClearEngine();
            if (SelectedEngine == null)
                return;

            try
            {
                var engine = SelectedEngine.Create(Auftraggeber);

                // enable automatic UI thread sync. and dispatcher invocation
                BindingOperations.EnableCollectionSynchronization(engine.DeliveredRockets, engine.DeliveredRockets);

                ActiveEngine = engine;
            }
            catch (Exception e)
            {
                Logger.Log("Engine creation: {0}", e.Message);
                SelectedEngine = null;
            }
        }

        public IAuftraggeberEngine ActiveEngine
        {
            get { return activeEngine; }
            set
            {
                if (activeEngine == value)
                    return;

                activeEngine = value;
                OnPropertyChanged();
            }
        }

        public int RocketCount { get; set; }

        public IEnumerable<Effektfarbe> AvailableColors
        {
            get { return Enum.GetValues(typeof(Effektfarbe)).Cast<Effektfarbe>(); }
        }

        public Effektfarbe SelectedColor1
        {
            get { return selectedColors[0]; }
            set { selectedColors[0] = value; }
        }
        public Effektfarbe SelectedColor2
        {
            get { return selectedColors[1]; }
            set { selectedColors[1] = value; }
        }

        public Effektfarbe SelectedColor3
        {
            get { return selectedColors[2]; }
            set { selectedColors[2] = value; }
        }

        public String DeliverySpace
        {
            get { return deliverySpace; }
            set
            {
                if (value == deliverySpace)
                    return;

                deliverySpace = value;
                OnPropertyChanged();
            }
        }

        public String Auftraggeber
        {
            get { return auftraggeber; }
            set
            {
                if (value == auftraggeber)
                    return;

                auftraggeber = value;
                OnPropertyChanged();
            }
        }

        public String DeliveryStorage
        {
            get { return deliveryStorage; }
            set
            {
                if (value == deliveryStorage)
                    return;

                deliveryStorage = value;
                OnPropertyChanged();
            }
        }

        public ICommand OrderCommand
        {
            get { return orderCommand ?? (orderCommand = new RelayCommand(p => Order())); }
        }

        private void Order()
        {
            var order = Auftrag.Create(Auftraggeber, DeliverySpace, DeliveryStorage, RocketCount, SelectedColor1, SelectedColor2, SelectedColor3);

            try
            {
                if (ActiveEngine != null)
                {
                    Logger.Log("Ordering: " + order);
                    ActiveEngine.SubmitOrder(order);
                }
            }
            catch (Exception e)
            {
                Logger.Log(e.Message);
            }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        private void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion

        #region IDisposable

        public void Dispose()
        {
            ClearEngine();
        }

        #endregion
    }
}
