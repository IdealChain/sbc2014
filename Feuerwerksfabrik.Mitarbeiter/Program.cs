﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Feuerwerksfabrik.Common;

namespace Feuerwerksfabrik.Mitarbeiter
{
    public class Program
    {
        static void Main(string[] args)
        {
            try
            {
                var console = new MitarbeiterConsole();
                Logger.LogAction += Console.WriteLine;
                console.Run(args);
            }
            catch (Exception e)
            {
                if (System.Diagnostics.Debugger.IsAttached)
                    throw;

                Console.WriteLine(e.ToString());
                Console.WriteLine();
                Console.WriteLine("Ooops, this worker is not well...");
                Console.WriteLine("Read above, and press enter to exit.");
                Console.ReadLine();
            }
        }
    }
}
