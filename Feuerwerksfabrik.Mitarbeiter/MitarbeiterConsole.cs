using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting.Messaging;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;

namespace Feuerwerksfabrik.Mitarbeiter
{
    public class MitarbeiterConsole
    {
        public MitarbeiterConsole()
        {
            CompositionInitializer.SatisfyImports(this);
        }

        public void Run(string[] args)
        {
            if (args.Length != 3)
                PrintUsage();
            else
                Run(args[0], args[1], args[2]);
        }

        public void PrintUsage()
        {
            Console.WriteLine(
@"Verwendung:
{0} <Fabrik> <Mitarbeiter> <ID>

Fabrik:      {1}
Mitarbeiter: {2}",
                Path.GetFileName(Assembly.GetExecutingAssembly().Location),
                String.Join(", ", EngineFactories.Select(f => f.Name)),
                String.Join(", ", MitarbeiterTypes.Select(t => t.Name)));

            if (System.Diagnostics.Debugger.IsAttached)
                Console.ReadLine();

        }

        public void Run(String engineName, String mitarbeiterName, String id)
        {
            var engine = EngineFactories.First(e => e.Name.EqualsIgnoreCase(engineName));
            var mitarbeiter = MitarbeiterTypes.First(t => t.Name.EqualsIgnoreCase(mitarbeiterName));
            Run(engine, mitarbeiter, id);
        }

        public void Run(IFeuerwerksfabrikFactory engine, Type mitarbeiter, String id)
        {
            var data = (Common.Actors.Mitarbeiter)Activator.CreateInstance(mitarbeiter);
            data.ID = id;

            if (id.ContainsIgnoreCase("bench"))
                BenchmarkMode = true;

            var task = engine.CreateMitarbeiterTask(data);
            task.RunSynchronously();
        }

        public static Boolean BenchmarkMode
        {
            set
            {
                if (value) Logger.Log("Benchmark mode!");
                Helpers.SleepRandomDisabled = value;
            }
        }

        public static String GetExecutableName()
        {
            return Assembly.GetExecutingAssembly().Location;
        }

        public static String ComposeArguments(IFeuerwerksfabrikFactory engine, Type mitarbeiter, String id)
        {
            return String.Format("{0} {1} {2}", engine.Name, mitarbeiter.Name, id);
        }

        /// <summary>
        /// Enumerates the currently available engine factories.
        /// Automatically injected via MEF.
        /// </summary>
        [ImportMany]
        public IEnumerable<IFeuerwerksfabrikFactory> EngineFactories { get; private set; }

        /// <summary>
        /// Mitarbeiter types available for creation.
        /// </summary>
        public readonly IEnumerable<Type> MitarbeiterTypes = new List<Type>
        {
            typeof (Produzent),
            typeof (QA),
            typeof (Logistiker)
        };

    }
}