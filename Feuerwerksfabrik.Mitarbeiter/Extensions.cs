﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.Mitarbeiter
{
    public static class Extensions
    {
        public static bool EqualsIgnoreCase(this String a, String b)
        {
            return a.ToLowerInvariant().Equals(b.ToLowerInvariant());
        }

        public static bool ContainsIgnoreCase(this String a, String b)
        {
            return a.ToLowerInvariant().Contains(b.ToLowerInvariant());
        }

    }
}
