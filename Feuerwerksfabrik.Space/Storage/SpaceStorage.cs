﻿using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Space.Properties;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using XcoSpaces.Exceptions;
using XcoSpaces.Kernel;
using XcoSpaces.Kernel.Selectors;

namespace Feuerwerksfabrik.Space.Storage
{
    [Serializable]
    public class SpaceStorage<T> : IDisposable
    {
        public static SpaceStorage<T> Create(String address, String name, params Selector[] selectors)
        {
            return new SpaceStorage<T>(address, name, selectors);
        }

        public static SpaceStorage<T> Create(String name, params Selector[] selectors)
        {
            return new SpaceStorage<T>(null, name, selectors);
        }

        public static SpaceStorage<T> Create(String name)
        {
            return new SpaceStorage<T>(null, name, (Selector[])null);
        }

        public static SpaceStorage<T> Get(String address, String name)
        {
            return new SpaceStorage<T>(address, name);
        }

        private ContainerReference container;
        private Notification takeListenerNotification = null;
        private Notification.NotificationReadCallback takeListener = null;
        private Notification putListenerNotification = null;
        private Notification.NotificationWriteCallback putListener = null;
        private bool disposing = false;
        private bool containerSelfCreated;

        private SpaceStorage(String address, String name, Selector[] selectors)
        {
            containerSelfCreated = true;
            if (selectors == null)
                container = XcoKernelSingleton.Instance.CreateNamedContainer(address, name, -1, new FifoSelector());
            else
                container = XcoKernelSingleton.Instance.CreateNamedContainer(address, name, -1, selectors);
        }

        private SpaceStorage(string address, string name)
        {
            container = XcoKernelSingleton.Instance.GetNamedContainer(address, name);
            containerSelfCreated = false;
        }

        public List<T> TakeList(TransactionReference transaction, int timeout, params Selector[] selectors)
        {
            List<IEntry> list;
            if (selectors == null)
                list = XcoKernelSingleton.Instance.Take(container, transaction, timeout, new FifoSelector(1));
            else
                list = XcoKernelSingleton.Instance.Take(container, transaction, timeout, selectors);

            List<T> retValue = new List<T>();
            foreach(IEntry entry in list) {
                retValue.Add((T)entry.Value);
            }
            return retValue;
        }

        public T Take(TransactionReference transaction, int timeout, params Selector[] selectors)
        {
            List<IEntry> list;
            if (selectors == null)
                list = XcoKernelSingleton.Instance.Take(container, transaction, timeout, new FifoSelector(1));
            else
                list = XcoKernelSingleton.Instance.Take(container, transaction, timeout, selectors);
            return ((T)list[0].Value);
        }

        public T Take(TransactionReference transaction)
        {
            return Take(transaction, Timeout.Infinite, null);
        }

        public T Take()
        {
            return Take(null, Timeout.Infinite, null);
        }

        public T Read(TransactionReference transaction, int timeout, params Selector[] selectors)
        {
            List<IEntry> list;
            if (selectors == null)
                list = XcoKernelSingleton.Instance.Read(container, transaction, timeout, new FifoSelector(1));
            else
                list = XcoKernelSingleton.Instance.Read(container, transaction, timeout, selectors);
            return ((T)list[0].Value);
        }

        public T Read(TransactionReference transaction)
        {
            return Read(transaction, Timeout.Infinite, null);
        }

        public T Read()
        {
            return Read(null, Timeout.Infinite, null);
        }

        public void Put(TransactionReference transaction, T[] values, params Selector[] selectors)
        {
            if (values == null)
                throw new ArgumentNullException();

            Entry[] entries = new Entry[values.Length];
            int count = 0;
            foreach (T value in values)
            {
                Entry entry = new Entry(value);
                if (selectors != null)
                {
                    foreach (Selector selector in selectors)
                        entry.Selectors.Add(selector);
                }
                entries[count] = entry;
                count++;
            }

            XcoKernelSingleton.Instance.Write(container, transaction, Timeout.Infinite, entries);
        }

        public void Put(TransactionReference transaction, T value, params Selector[] selectors)
        {
            T[] values = { value };
            Put(transaction, values, selectors);
        }

        public void Put(T value)
        {
            Put(null, value, null);
        }

        public void Put(TransactionReference transaction, T value)
        {
            Put(transaction, value, null);
        }

        public void Overwrite(TransactionReference transaction, T value, params Selector[] selectors)
        {
            if (value == null)
                throw new ArgumentNullException();

            Entry entry = new Entry(value);
            if (selectors != null)
            {
                foreach (Selector selector in selectors)
                    entry.Selectors.Add(selector);
            }

            XcoKernelSingleton.Instance.Shift(container, transaction, Timeout.Infinite, entry);
        }

        public void Overwrite(T value)
        {
            Overwrite(null, value, null);
        }

        public void Overwrite(TransactionReference transaction, T value)
        {
            Overwrite(transaction, value, null);
        }

        public void setTakeListener(Notification.NotificationReadCallback listener)
        {
            takeListener = listener;
            takeListenerNotification = XcoKernelSingleton.Instance.CreateReadNotification(container, null, ReadOperation.Take);
            takeListenerNotification.SetReadCallback(takeListener);
            takeListenerNotification.Start();
        }

        public void clearTakeListner()
        {
            if (takeListenerNotification != null)
            {
                takeListenerNotification.Stop();
                takeListenerNotification = null;
            }
            takeListener = null;
        }

        public void setPutListener(Notification.NotificationWriteCallback listener)
        {
            putListener = listener;
            putListenerNotification = XcoKernelSingleton.Instance.CreateWriteNotification(container, null, WriteOperation.Write);
            putListenerNotification.SetWriteCallback(putListener);
            putListenerNotification.Start();
        }

        public void clearPutListner()
        {
            if (putListenerNotification != null)
            {
                putListenerNotification.Stop();
                putListenerNotification = null;
            }
            putListener = null;
        }


        #region IDisposable
        public void Dispose()
        {
            if (!disposing)
            {
                disposing = true;
                if (takeListenerNotification != null)
                {
                    takeListenerNotification.Stop();
                    takeListenerNotification = null;
                }
                if (putListenerNotification != null)
                {
                    putListenerNotification.Stop();
                    putListenerNotification = null;
                }
                if (containerSelfCreated)
                {
                    try
                    {
                        if (XcoKernelSingleton.Instance != null && container != null)
                            XcoKernelSingleton.Instance.DestroyContainer(container);
                    }
                    catch (XcoException ex)
                    {
                        //nothing to do, only catched because of the many different errors which can occur here while shutting down
                    }
                }
                container = null;
                Logger.Log("Storage disposed");
            }
        }

        #endregion IDisposable
    }
}
