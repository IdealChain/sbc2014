﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XcoSpaces.Kernel;

namespace Feuerwerksfabrik.Space
{
    public static class XcoKernelSingleton
    {
        private static XcoKernel instance;

        public static XcoKernel Instance
        {
            get { return instance ?? (instance = new XcoKernel()); }
        }

        public static void Dispose()
        {
            if (instance != null)
            {
                instance.Dispose();
                instance = null;
            }
        }
    }
}
