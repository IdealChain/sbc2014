﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;
using Feuerwerksfabrik.Common.Objects;
using Feuerwerksfabrik.Space.Properties;
using Feuerwerksfabrik.Space.Storage;
using XcoSpaces.Kernel;
using XcoSpaces.Kernel.Selectors;

namespace Feuerwerksfabrik.Space
{
    internal class SpaceEngine : IFeuerwerksfabrikEngine
    {
        private readonly Dictionary<Type, SpaceStorage<Bestandteil>> anlieferung = new Dictionary
            <Type, SpaceStorage<Bestandteil>>
        {
            {typeof(Holzstab), SpaceStorage<Bestandteil>.Create(typeof(Holzstab).Name)},
            {typeof(Gehaeuse), SpaceStorage<Bestandteil>.Create(typeof(Gehaeuse).Name)},
            {typeof(Treibladungspackung), SpaceStorage<Bestandteil>.Create(typeof(Treibladungspackung).Name, new FifoSelector(), new LabelSelector<string>("Stand"))},
            {typeof(Effektladung), SpaceStorage<Bestandteil>.Create(typeof(Effektladung).Name, new FifoSelector(), new LabelSelector<string>("Farbe"))},
        };

        private readonly SpaceStorage<Auftrag> AuftragsStorage = SpaceStorage<Auftrag>.Create(Settings.Default.ContainerName_AuftragsStorage, new FifoSelector(), new KeySelector<string>("id"), new LabelSelector<string>("status"));

        private readonly SpaceStorage<Rakete> FinishedRocketsStorage = SpaceStorage<Rakete>.Create(Settings.Default.ContainerName_FinishedRocketsStorage);
        private readonly SpaceStorage<Rakete> VerifiedRocketsStorage = SpaceStorage<Rakete>.Create(Settings.Default.ContainerName_VerifiedRocketsStorage, new FifoSelector(), new LabelSelector<string>("Qualitaet"), new LabelSelector<string>("Auftrag"));
        private readonly SpaceStorage<Rakete> DefectRocketsStorage = SpaceStorage<Rakete>.Create(Settings.Default.ContainerName_DefectRocketsStorage);
        private readonly SpaceStorage<Raketenpackung> DeliveredRocketPackagesStorage = SpaceStorage<Raketenpackung>.Create(Settings.Default.ContainerName_PackagedRocketsStorage);

        private bool disposing;

        public SpaceEngine()
        {
            Auftraege = new ObservableCollection<Auftrag>();
            AuftragsStatus = new ObservableCollection<int>();
            AvailableParts = new ObservableCollection<Bestandteil>();
            FinishedRockets = new ObservableCollection<Rakete>();
            VerifiedRockets = new ObservableCollection<Rakete>();
            DeliveredRocketPackages = new ObservableCollection<Raketenpackung>();
            DefectRockets = new ObservableCollection<Rakete>();

            try
            {
                Logger.Log("Starting space on port {0}", Settings.Default.SpacePort);
                XcoKernelSingleton.Instance.Start(Settings.Default.SpacePort);

                // monitor delivered parts
                foreach (var type in anlieferung.Keys)
                {
                    var storage = anlieferung[type];
                    storage.setPutListener(((source, operation, entries) =>
                    {
                        lock (AvailableParts)
                        {
                            foreach (IEntry entry in entries)
                                AvailableParts.Add((Bestandteil) entry.Value);
                        }
                    }));
                    storage.setTakeListener(((source, operation, entries) =>
                    {
                        lock (AvailableParts)
                            foreach (IEntry entry in entries)
                                AvailableParts.Remove((Bestandteil) entry.Value);
                    }));
                }

                MonitorList(Auftraege, AuftragsStorage);
                MonitorList(FinishedRockets, FinishedRocketsStorage);
                MonitorList(VerifiedRockets, VerifiedRocketsStorage);
                MonitorList(DefectRockets, DefectRocketsStorage);
                MonitorList(DeliveredRocketPackages, DeliveredRocketPackagesStorage);

                SpaceStorage<Rakete> sss = SpaceStorage<Rakete>.Create("depot_auftraggeber-000");
                /*Rakete rakete = new Rakete();
                rakete.ID = "hallo";
                sss.Put(rakete);*/

                //Auftrag tmpAuftrag = Auftrag.Create("auftraggeber-000", "127.0.0.1:25555", "deliverStorage", 1, Effektfarbe.Rot, Effektfarbe.Rot, Effektfarbe.Rot);
                //AuftragsStorage.Put(null, tmpAuftrag, new FifoSelector(), new KeySelector<string>("id", tmpAuftrag.ID));
                //AuftragsStatusStorage.Put(null, 0, new KeySelector<string>("status", tmpAuftrag.ID));
            }
            catch (Exception)
            {
                Dispose();
                throw;
            }
        }

        private static void MonitorList<T>(ObservableCollection<T> list, SpaceStorage<T> spaceStorage)
        {
            spaceStorage.setPutListener((source, operation, entries) =>
                {
                    lock (list)
                    {
                        foreach (IEntry entry in entries)
                            list.Add((T)entry.Value);
                    }
                });
            spaceStorage.setTakeListener((source, operation, entries) =>
                {
                    lock (list)
                    {
                        foreach (IEntry entry in entries)
                            list.Remove((T)entry.Value);
                    }
                });
        }

        public Task CreateLieferantTask(Lieferant data, CancellationToken token)
        {
            return new Task(() =>
            {
                try
                {
                    var count = data.Count;
                    while (count > 0 && !token.IsCancellationRequested)
                    {
                        Helpers.SleepRandom(1000, 2000);

                        var part = Bestandteil.Create(data);
                        Logger.Log("{0} delivering {1}", data.ID, part);
                        if (data.BestandteilType == typeof(Treibladungspackung))
                            anlieferung[data.BestandteilType].Put(null, part,
                                new LabelSelector<string>("Stand", ((Treibladungspackung)part).Stand.ToString()));
                        else if (data.BestandteilType == typeof(Effektladung))
                            anlieferung[data.BestandteilType].Put(null, part,
                                new LabelSelector<string>("Farbe", ((Effektladung)part).Farbe.ToString()));
                        else
                            anlieferung[data.BestandteilType].Put(part);

                        count--;
                    }
                    Logger.Log("{0} {1}", data.ID, token.IsCancellationRequested ? "canceled" : "done");
                }
                catch (Exception)
                {
                    if (!disposing)
                        throw;
                }
            });
        }

        public ObservableCollection<Auftrag> Auftraege { get; private set; }

        public ObservableCollection<int> AuftragsStatus { get; private set; }

        public ObservableCollection<Bestandteil> AvailableParts { get; private set; }

        public ObservableCollection<Rakete> FinishedRockets { get; private set; }

        public ObservableCollection<Rakete> VerifiedRockets { get; private set; }

        public ObservableCollection<Raketenpackung> DeliveredRocketPackages { get; private set; }

        public ObservableCollection<Rakete> DefectRockets { get; private set; }

        #region IDisposable

        public void Dispose()
        {
            if (!disposing)
            {
                disposing = true;
                Logger.Log("Disposing, stopping space");
                foreach (var type in anlieferung.Keys)
                {
                    anlieferung[type].clearPutListner();
                    anlieferung[type].clearTakeListner();
                }
                anlieferung.Clear();

                FinishedRocketsStorage.Dispose();
                VerifiedRocketsStorage.Dispose();
                DefectRocketsStorage.Dispose();
                DeliveredRocketPackagesStorage.Dispose();

                XcoKernelSingleton.Dispose();
            }
        }

        #endregion
    }
}
