using System;
using System.Configuration;
using System.Collections.Generic;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;
using Feuerwerksfabrik.Common.Objects;
using Feuerwerksfabrik.Space.Properties;
using Feuerwerksfabrik.Space.Storage;
using XcoSpaces.Kernel;
using XcoSpaces.Kernel.Selectors;
using System.Threading;
using XcoSpaces.Exceptions;

namespace Feuerwerksfabrik.Space.Tasks
{
    public class LogistikerWorker : WorkerBase<Logistiker>, IDisposable
    {
        private static readonly int CONST_AUFTRAGWORK_TRANSACTION_TIMEOUT = 2500;
        private static readonly int CONST_AUFTRAGWORK_REMOTETRANSACTION_TIMEOUT = 2500;
        private static readonly int CONST_AUFTRAGWORK_TAKEAUFTRAG_TIMEOUT = 100;
        private static readonly int CONST_AUFTRAGWORK_TAKEROCKET_TIMEOUT = 50;
        private static readonly int CONST_STOREINDEPOTWORK_TRANSACTION_TIMEOUT = 2500;
        private static readonly int CONST_STOREINDEPOTWORK_TAKEAUFTRAG_TIMEOUT = 100;
        private static readonly int CONST_STOREINDEPOTWORK_TAKEROCKET_TIMEOUT = 50;
        private static readonly int CONST_CLEANUP_TRANSACTION_TIMEOUT = 2500;
        private static readonly int CONST_CLEANUP_TAKEROCKET_TIMEOUT = 50;
        private static readonly int CONST_DEFAULTWORK_TRANSACTION_TIMEOUT = 2500;
        private static readonly int CONST_DEFAULTWORK_TAKEROCKET_TIMEOUT = 100;
        private static readonly int CONST_DEFAULTWORK_TAKEROCKETLIST_TIMEOUT = 600;
        
        private static readonly int RAKETEN_PER_PACKAGE = 5;

        public static LogistikerWorker Create(Logistiker logistiker)
        {
            LogistikerWorker logistikerWorker = new LogistikerWorker { logistiker = logistiker };
            logistikerWorker.AuftragsStorage = SpaceStorage<Auftrag>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, Settings.Default.ContainerName_AuftragsStorage);
            logistikerWorker.verifiedRocketsStorage = SpaceStorage<Rakete>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, Settings.Default.ContainerName_VerifiedRocketsStorage);
            logistikerWorker.defectRocketsStorage = SpaceStorage<Rakete>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, Settings.Default.ContainerName_DefectRocketsStorage);
            logistikerWorker.packagedRocketsStorage = SpaceStorage<Raketenpackung>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, Settings.Default.ContainerName_PackagedRocketsStorage);
            return logistikerWorker;
        }

        private bool disposing = false;
        private Logistiker logistiker;
        private TransactionReference transaction;
        private SpaceStorage<Auftrag> AuftragsStorage;
        private SpaceStorage<Rakete> verifiedRocketsStorage;
        private SpaceStorage<Rakete> defectRocketsStorage;
        private SpaceStorage<Raketenpackung> packagedRocketsStorage;

        public override void Run()
        {
            Logger.Log("{0}: Starting", logistiker.ID);

            try
            {
                while (!disposing)
                {
                    try
                    {
                        doCleanUpWork();
                        doAuftragsWork();
                        doDefaultWork();
                    }
                    catch (XcoSpaces.Exceptions.XcoTransactionException ex)
                    {
                        //this exception means that the transaction is already rolled back, usually because it timed out.
                        //This means, we have to begin a new working-loop, so we just ignore the error here
                        Logger.Log("{0}: Transaction timed out, starting new work", logistiker.ID);
                    }
                }
            }
            catch (XcoSpaces.Exceptions.XcoOperationTimeoutException ex)
            {
                if (transaction != null) {
                    try
                    {
                        XcoKernelSingleton.Instance.RollbackTransaction(transaction);
                    }
                    catch (XcoSpaces.Exceptions.XcoTransactionException)
                    {
                        //nothing to do
                    }
                }
                //ignore the error if the take-operation is interrupted because the System is shutting down
                if (!ex.Message.Equals("Core shut down."))
                    throw;
            }
            catch (XcoSpaces.Exceptions.XcoContainerNotFoundException ex)
            {
                //this exception means the space (or a necessary container) can't be reached - usually this happens if the space is shutting down
                XcoKernelSingleton.Instance.RollbackTransaction(transaction);
            }
            catch (Exception ex)
            {
                Logger.Log("{0}: unexpected exception: {1}", logistiker.ID, ex);
            }
        }

        private void doCleanUpWork()
        {
            TransactionReference cleaningTransaction = null;
            try
            {
                Logger.Log("{0}: Cleaning up", logistiker.ID);
                while (true)
                {
                    cleaningTransaction = XcoKernelSingleton.Instance.CreateTransaction(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, CONST_CLEANUP_TRANSACTION_TIMEOUT);
                    Rakete defectRocket = verifiedRocketsStorage.Take(cleaningTransaction, CONST_CLEANUP_TAKEROCKET_TIMEOUT, new LabelSelector<string>("Qualitaet", Raketenqualitaet.Defekt.ToString()));
                    Logger.Log("{0}: Trashing {1}", logistiker.ID, defectRocket.ID);
                    defectRocket.Mitarbeiter.Add(logistiker);
                    defectRocketsStorage.Put(cleaningTransaction, defectRocket);
                    XcoKernelSingleton.Instance.CommitTransaction(cleaningTransaction);
                }
            }
            catch (XcoSpaces.Exceptions.XcoOperationFailedException ex)
            {
                if (ex.Message.StartsWith("Operation timed out"))
                {
                    //there are no more defekt rockets in the storage, we can continue our work
                }
                else
                {
                    if (cleaningTransaction != null)
                        XcoKernelSingleton.Instance.RollbackTransaction(cleaningTransaction);
                    throw ex;
                }
            }
            catch (XcoSpaces.Exceptions.XcoTransactionException)
            {
                //ok, no more defect rockets to collect, go to the real work
            }
        }

        private void doAuftragsWork()
        {
            TransactionReference deliveringTransaction = null;
            TransactionReference deliveringRemoteTransaction = null;
            try
            {
                Logger.Log("{0}: Checking for Orders to deliver", logistiker.ID);
                while (true)
                {
                    deliveringTransaction = XcoKernelSingleton.Instance.CreateTransaction(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, CONST_AUFTRAGWORK_TRANSACTION_TIMEOUT);
                    Logger.Log("{0}: trying to get Order", logistiker.ID);
                    Auftrag auftrag = AuftragsStorage.Take(deliveringTransaction, CONST_AUFTRAGWORK_TAKEAUFTRAG_TIMEOUT, new LabelSelector<string>("status", AuftragsStatus.Erledigt.ToString()));

                    Logger.Log("{0}: getting RemoteTransaction", logistiker.ID);
                    deliveringRemoteTransaction = XcoKernelSingleton.Instance.CreateTransaction(auftrag.deliverAddress, CONST_AUFTRAGWORK_REMOTETRANSACTION_TIMEOUT);
                    Logger.Log("{0}: getting RemoteStorage", logistiker.ID);
                    SpaceStorage<Rakete> remoteStorage = SpaceStorage<Rakete>.Get(auftrag.deliverAddress, auftrag.deliverStorage);
                    if (auftrag.anzahlBereitsUeberprueft >= auftrag.anzahl)
                    {
                        List<Rakete> rockets = verifiedRocketsStorage.TakeList(deliveringTransaction, CONST_AUFTRAGWORK_TAKEROCKET_TIMEOUT, new LabelSelector<string>(auftrag.anzahlBereitsUeberprueft, "Auftrag", auftrag.ID.ToString()));
                        foreach (Rakete r in rockets)
                        {
                            Logger.Log("{0}: taking ordered Rocket from verifiedStorage", logistiker.ID);
                            r.Mitarbeiter.Add(logistiker);
                        }
                        remoteStorage.Put(deliveringRemoteTransaction, rockets.ToArray(), new FifoSelector());
                        auftrag.Status = AuftragsStatus.Ausgeliefert;
                        Helpers.SleepRandom(2000, 2000);
                    }
                    Logger.Log("{0}: Delivering {1}", logistiker.ID, auftrag.ID);
                    AuftragsStorage.Put(deliveringTransaction, auftrag, new FifoSelector(), new KeySelector<string>("id", auftrag.ID), new LabelSelector<string>("status", auftrag.Status.ToString()));
                    XcoKernelSingleton.Instance.CommitTransaction(deliveringTransaction);
                    XcoKernelSingleton.Instance.CommitTransaction(deliveringRemoteTransaction);
                }
            }
            catch (XcoSpaces.Exceptions.XcoTransactionException)
            {
                //no Orders to process - nothing to do here
                if (deliveringTransaction != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(deliveringTransaction);
                if (deliveringRemoteTransaction != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(deliveringRemoteTransaction);
                return;
            }
            catch (XcoOperationTimeoutException ex)
            {
                //Logger.Log("{0}: catching timeout: ex={1}", logistiker.ID, ex);
                //there are no Orders to process, we can continue our work
                if (deliveringTransaction != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(deliveringTransaction);
                if (deliveringRemoteTransaction != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(deliveringRemoteTransaction);
            }
            catch (XcoSpaces.Exceptions.XcoOperationFailedException ex)
            {
                if (deliveringTransaction != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(deliveringTransaction);
                if (deliveringRemoteTransaction != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(deliveringRemoteTransaction);
            }
            catch (XcoSpaces.Exceptions.XcoCommunicationException ex)
            {
                //Logger.Log("{0}: catching communication: ex={1}", logistiker.ID, ex);
                //can't contact remote space!
                if (deliveringTransaction != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(deliveringTransaction);
                if (deliveringRemoteTransaction != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(deliveringRemoteTransaction);
                storeAuftragInDepot();
            }
            catch (Exception ex)
            {
                if (deliveringTransaction != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(deliveringTransaction);
                if (deliveringRemoteTransaction != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(deliveringRemoteTransaction);
                Logger.Log("ex = " + ex);
                //throw ex;
            }
        }

        private void storeAuftragInDepot()
        {
            TransactionReference deliveringTransaction2 = null;
            try
            {
                SpaceStorage<Rakete> AuftraggeberStorage = null;
                Logger.Log("{0}: Checking for Orders to put in depot", logistiker.ID);

                deliveringTransaction2 = XcoKernelSingleton.Instance.CreateTransaction(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, CONST_STOREINDEPOTWORK_TRANSACTION_TIMEOUT);
                Logger.Log("{0}: depot: trying to get auftrag", logistiker.ID);
                Auftrag auftrag = AuftragsStorage.Take(deliveringTransaction2, CONST_STOREINDEPOTWORK_TAKEAUFTRAG_TIMEOUT, new LabelSelector<string>("status", AuftragsStatus.Erledigt.ToString()));
                Logger.Log("{0}: depot: got auftrag {1}", logistiker.ID, auftrag.ID);

                string depotName = "depot_" + auftrag.auftraggeber;
                try
                {
                    AuftraggeberStorage = SpaceStorage<Rakete>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, depotName);
                    Logger.Log("{0}: got depot: {1}", logistiker.ID, depotName);
                }
                catch (XcoSpaces.Exceptions.XcoContainerNotFoundException ex2)
                {
                    //container doesn't exist yet, create it:
                    Logger.Log("{0}: created depot: {1}", logistiker.ID, depotName);
                    AuftraggeberStorage = SpaceStorage<Rakete>.Create(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, depotName, new FifoSelector());
                }
                for (int i = 0; i < auftrag.anzahlBereitsUeberprueft; i++)
                {
                    Logger.Log("{0}: taking Rocket from verifiedStorage", logistiker.ID);
                    Rakete rocket = verifiedRocketsStorage.Take(deliveringTransaction2, CONST_STOREINDEPOTWORK_TAKEROCKET_TIMEOUT, new LabelSelector<string>("Auftrag", auftrag.ID));
                    rocket.Mitarbeiter.Add(logistiker);
                    Logger.Log("{0}: placing rocket {1} in depot of {2}", logistiker.ID, rocket.ID, auftrag.auftraggeber);
                    AuftraggeberStorage.Put(deliveringTransaction2, rocket);
                }
                auftrag.Status = AuftragsStatus.Aufbewahrt;
                AuftragsStorage.Put(transaction, auftrag, new FifoSelector(), new KeySelector<string>("id", auftrag.ID), new LabelSelector<string>("status", auftrag.Status.ToString()));
                XcoKernelSingleton.Instance.CommitTransaction(deliveringTransaction2);
            }
            catch (XcoSpaces.Exceptions.XcoOperationTimeoutException)
            {
                if (deliveringTransaction2 != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(deliveringTransaction2);
                return;
            }
            catch (XcoSpaces.Exceptions.XcoTransactionException)
            {
                if (deliveringTransaction2 != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(deliveringTransaction2);
                return;
            }
            catch (XcoOperationFailedException ex2)
            {
                //Logger.Log("XcoOperationFailedException: ex=" + ex2);
                //Logger.Log("{0}: catching timeout: ex={1}", logistiker.ID, ex);
                //there are no Orders to process, we can continue our work
                if (deliveringTransaction2 != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(deliveringTransaction2);
            }
            catch (XcoException ex2)
            {
                Logger.Log("unexpected exception: ex=" + ex2);
                throw;
            }
        }

        private void doDefaultWork()
        {
            try
            {
                Logger.Log("{0}: Doing default-work", logistiker.ID);
                transaction = XcoKernelSingleton.Instance.CreateTransaction(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, CONST_DEFAULTWORK_TRANSACTION_TIMEOUT);

                Raketenpackung packung = Raketenpackung.Create(logistiker);

                Rakete firstRocket = verifiedRocketsStorage.Read(transaction, CONST_DEFAULTWORK_TAKEROCKET_TIMEOUT, new LabelSelector<string>(1, "Auftrag", "_"));

                //List<Rakete> firstRocketL = verifiedRocketsStorage.TakeList(transaction, Timeout.Infinite, new LabelSelector<string>(1, "Qualitaet", Raketenqualitaet.KlasseB.ToString()));
                //new LabelSelector<string>(1, "Auftrag", "_")
                //Logger.Log("{0}: ### Anzahl Eintraege = {1}", logistiker.ID, firstRocketL.Count);
                //Rakete firstRocket = firstRocketL[0];
                Logger.Log("{0}: got first rocket {1} with quality {2}, collecting for package", logistiker.ID, firstRocket.ID, firstRocket.Qualitaet.ToString());

                List<Rakete> verifiedRocketsList;
                verifiedRocketsList = verifiedRocketsStorage.TakeList(transaction, CONST_DEFAULTWORK_TAKEROCKETLIST_TIMEOUT, new LabelSelector<string>(RAKETEN_PER_PACKAGE, "Qualitaet", firstRocket.Qualitaet.ToString()));
                packung.Qualitaet = verifiedRocketsList[0].Qualitaet;
                //verifiedRocketsList.Add(firstRocket);

                foreach (Rakete r in verifiedRocketsList)
                {
                    Logger.Log("{0}: Adding {1} to {2}", logistiker.ID, r.ID, packung.ID);
                    r.Mitarbeiter.Add(logistiker);
                    packung.Raketen.Add(r);
                }

                Logger.Log("{0}: Finished {1}", logistiker.ID, packung.ID);
                packagedRocketsStorage.Put(transaction, packung);

                XcoKernelSingleton.Instance.CommitTransaction(transaction);
            }
            catch (XcoSpaces.Exceptions.XcoOperationTimeoutException ex)
            {
                if (transaction != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(transaction);
                return;
            }
            catch (XcoSpaces.Exceptions.XcoTransactionException)
            {
                if (transaction != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(transaction);
                return;
            }
            catch (XcoSpaces.Exceptions.XcoOperationFailedException)
            {
                if (transaction != null)
                    XcoKernelSingleton.Instance.RollbackTransaction(transaction);
                return;
            }
            catch (Exception ex)
            {
                Logger.Log("{0}: unexpected exception: {1}", logistiker.ID, ex);
                throw ex;
            }
        }

        public override void Dispose()
        {
            if (!disposing)
            {
                disposing = true;

                Logger.Log("Disposing, stopping LogistikerWorker {0}", logistiker.ID);
                AuftragsStorage.Dispose();
                verifiedRocketsStorage.Dispose();
                defectRocketsStorage.Dispose();
                packagedRocketsStorage.Dispose();

                base.Dispose();
            }
        }
    }
}