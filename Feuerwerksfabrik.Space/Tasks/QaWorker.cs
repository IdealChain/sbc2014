﻿using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;
using Feuerwerksfabrik.Common.Objects;
using Feuerwerksfabrik.Space.Properties;
using Feuerwerksfabrik.Space.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using XcoSpaces.Kernel;
using XcoSpaces.Kernel.Selectors;

namespace Feuerwerksfabrik.Space.Tasks
{
    public class QaWorker : WorkerBase<QA>, IDisposable
    {
        private static readonly int CONST_QAWORKER_TRANSACTION_TIMEOUT = 2000;
        private static readonly int CONST_TAKEAUFTRAG_TIMEOUT = Timeout.Infinite;

        public static QaWorker Create(QA qa)
        {
            QaWorker qaworker = new QaWorker { qa = qa };
            qaworker.AuftragsStorage = SpaceStorage<Auftrag>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, Settings.Default.ContainerName_AuftragsStorage);
            qaworker.FinishedRocketsStorage = SpaceStorage<Rakete>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, Settings.Default.ContainerName_FinishedRocketsStorage);
            qaworker.VerifiedRocketsStorage = SpaceStorage<Rakete>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, Settings.Default.ContainerName_VerifiedRocketsStorage);
            return qaworker;
        }

        private QA qa;
        private TransactionReference transaction;
        private SpaceStorage<Auftrag> AuftragsStorage;
        private SpaceStorage<Rakete> FinishedRocketsStorage;
        private SpaceStorage<Rakete> VerifiedRocketsStorage;
        private bool disposing = false;

        public override void Run()
        {
            Logger.Log("{0}: Starting", qa.ID);

            while (!disposing)
            {
                try
                {
                    CheckRockets();
                }
                catch (XcoSpaces.Exceptions.XcoTransactionException)
                {
                    //this exception means that the transaction is already rolled back, usually because it timed out.
                    //This means, we have to begin a new working-loop, so we just ignore the error here
                    Logger.Log("{0}: Transaction timed out, starting new work", qa.ID);
                }
            }
        }

        private void CheckRockets()
        {
            try
            {
                transaction = XcoKernelSingleton.Instance.CreateTransaction(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, CONST_QAWORKER_TRANSACTION_TIMEOUT);

                Rakete currentRocket = FinishedRocketsStorage.Take(transaction);
                currentRocket.DetermineQuality();
                currentRocket.Mitarbeiter.Add(qa);

                if (currentRocket.Auftrag != null)
                {
                    Logger.Log("{0}: trying to get Auftrag-Details for current Rakete {0}: {1}", qa.ID, currentRocket.ID, currentRocket.Auftrag);
                    Auftrag auftrag = AuftragsStorage.Take(transaction, CONST_TAKEAUFTRAG_TIMEOUT, new KeySelector<string>("id", currentRocket.Auftrag.ID));
                    Logger.Log("{0}: got Auftrag-Details for current Rakete {0}: {1}", qa.ID, currentRocket.ID, auftrag);

                    if (currentRocket.Qualitaet == Raketenqualitaet.Defekt || currentRocket.Qualitaet == Raketenqualitaet.KlasseB) {
                        Logger.Log("{0}: Quality of {1} is {2}, removing from Order", qa, currentRocket.ID, currentRocket.Qualitaet);
                        auftrag.anzahlBereitsGebaut--;
                    }
                    else {
                        Logger.Log("{0}: Quality of {1} is ok", qa, currentRocket.ID, currentRocket.Qualitaet);
                        auftrag.anzahlBereitsUeberprueft++;
                    }

                    if (auftrag.anzahlBereitsUeberprueft >= auftrag.anzahl)
                    {
                        auftrag.Status = AuftragsStatus.Erledigt;
                    }
                    AuftragsStorage.Put(transaction, auftrag, new FifoSelector(), new KeySelector<string>("id", auftrag.ID), new LabelSelector<string>("status", auftrag.Status.ToString()));
                    Logger.Log("{0}: returned Auftrag-Details for current Rakete {0}", qa.ID, currentRocket.ID);
                }

                Logger.Log("{0}: Checked rocket {1}: {2}", qa.ID, currentRocket.ID, currentRocket.Qualitaet.ToString());
                if (currentRocket.Auftrag != null && currentRocket.Qualitaet == Raketenqualitaet.KlasseA)
                    VerifiedRocketsStorage.Put(transaction, currentRocket, new FifoSelector(), new LabelSelector<string>("Auftrag", currentRocket.Auftrag.ID.ToString()), new LabelSelector<string>("Qualitaet", "_"));
                else
                    VerifiedRocketsStorage.Put(transaction, currentRocket, new FifoSelector(), new LabelSelector<string>("Auftrag", "_"), new LabelSelector<string>("Qualitaet", currentRocket.Qualitaet.ToString()));

                XcoKernelSingleton.Instance.CommitTransaction(transaction);
            }
            catch (XcoSpaces.Exceptions.XcoOperationTimeoutException ex)
            {
                XcoKernelSingleton.Instance.RollbackTransaction(transaction);
                FinishedRocketsStorage.Dispose();
                VerifiedRocketsStorage.Dispose();

                //ignore the error if the take-operation is interrupted because the System is shutting down
                if (!ex.Message.Equals("Core shut down."))
                    throw;
                //if the core is shutting down, we should clean up:
                Logger.Log("{0}: Core is shutting down, exiting CheckRockets()", qa.ID);
                Dispose();
            }
            catch (XcoSpaces.Exceptions.XcoContainerNotFoundException)
            {
                //this exception means the space (or a necessary container) can't be reached - usually this happens if the space is shutting down
                Logger.Log("{0}: Container not found, exiting CheckRockets()", qa.ID);

                //cleaning up:
                XcoKernelSingleton.Instance.RollbackTransaction(transaction);
                FinishedRocketsStorage.Dispose();
                VerifiedRocketsStorage.Dispose();
                Dispose();
            }
            finally
            {
                XcoKernelSingleton.Instance.RollbackTransaction(transaction);
            }
        }

        public override void Dispose()
        {
            if (!disposing)
            {
                disposing = true;

                Logger.Log("Disposing, stopping QaWorker {0}", qa.ID);
                AuftragsStorage.Dispose();
                FinishedRocketsStorage.Dispose();
                VerifiedRocketsStorage.Dispose();

                base.Dispose();
            }
        }
    }
}
