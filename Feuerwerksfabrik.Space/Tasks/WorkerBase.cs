﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using XcoSpaces.Exceptions;

namespace Feuerwerksfabrik.Space.Tasks
{
    public abstract class WorkerBase<TMitarbeiter> : IDisposable where TMitarbeiter : Mitarbeiter
    {
        protected WorkerBase()
        {
            try
            {
                Logger.Log("Setting XcoKernel up for remote communication...");
                XcoKernelSingleton.Instance.Start(0);
            }
            catch (XcoCommunicationException e)
            {
                Logger.Log("Ignoring: " + e.Message);
            }
        }

        public abstract void Run();

        public virtual void Dispose()
        { }
    }
}
