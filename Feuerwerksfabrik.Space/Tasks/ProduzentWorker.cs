﻿using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;
using Feuerwerksfabrik.Common.Objects;
using Feuerwerksfabrik.Space.Properties;
using Feuerwerksfabrik.Space.Storage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using XcoSpaces;
using XcoSpaces.Collections;
using XcoSpaces.Kernel;
using XcoSpaces.Kernel.Selectors;

namespace Feuerwerksfabrik.Space.Tasks
{
    public class ProduzentWorker : WorkerBase<Produzent>
    {
        private static readonly int CONST_PRODUZENTWORKER_TRANSACTION_TIMEOUT = 2500;
        private static readonly int CONST_MAX_TIME_TO_WAIT_FOR_USED_TREIBLADUNGSPACKUNG = 100;
        private static readonly int CONST_MAX_TIME_TO_WAIT_FOR_AUFTRAG = 50;

        private Produzent produzent;
        private Auftrag auftrag;
        private SpaceStorage<Auftrag> AuftragsStorage;
        private SpaceStorage<Bestandteil> holzstabStorage;
        private SpaceStorage<Bestandteil> gehaeuseStorage;
        private SpaceStorage<Bestandteil> treibladungspackungStorage;
        private SpaceStorage<Bestandteil> effektladungStorage;
        private SpaceStorage<Rakete> finishedRocketsStorage;

        private readonly Random Random = new Random();
        private bool disposing = false;
        private TransactionReference transaction;

        public static ProduzentWorker Create(Produzent produzent)
        {
            ProduzentWorker worker = new ProduzentWorker();
            worker.produzent = produzent;

            // get containers from space
            worker.AuftragsStorage = SpaceStorage<Auftrag>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, Settings.Default.ContainerName_AuftragsStorage);
            worker.holzstabStorage = SpaceStorage<Bestandteil>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, typeof(Holzstab).Name);
            worker.gehaeuseStorage = SpaceStorage<Bestandteil>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, typeof(Gehaeuse).Name);
            worker.treibladungspackungStorage = SpaceStorage<Bestandteil>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, typeof(Treibladungspackung).Name);
            worker.effektladungStorage = SpaceStorage<Bestandteil>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, typeof(Effektladung).Name);
            worker.finishedRocketsStorage = SpaceStorage<Rakete>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, Settings.Default.ContainerName_FinishedRocketsStorage);

            return worker;
        }

        public override void Run()
        {
            while (!disposing)
            {
                try
                {
                    BuildNewRocket();
                }
                catch (XcoSpaces.Exceptions.XcoTransactionException)
                {
                    //this exception means that the transaction is already rolled back, usually because it timed out.
                    //This means, we have to begin a new working-loop, so we just ignore the error here
                    Logger.Log("{0}: Transaction timed out, starting new work", produzent.ID);
                }

            }
        }

        private void BuildNewRocket()
        {
            Logger.Log("{0}: Begin to build a new rocket", produzent.ID);

            // new rocket
            var rocket = Rakete.Create(produzent);

            try
            {
                transaction = XcoKernelSingleton.Instance.CreateTransaction(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, CONST_PRODUZENTWORKER_TRANSACTION_TIMEOUT);

                auftrag = tryToGetAuftrag();
                rocket.Auftrag = auftrag;

                Logger.Log("{0}: trying to collect parts", produzent.ID);
                var holzstabTask = Task.Factory.StartNew((Func<Holzstab>)takeHolzstabFromStorage);
                var gehauseTask = Task.Factory.StartNew((Func<Gehaeuse>)takeGehaeuseFromStorage);
                var treibladungsTask = Task.Factory.StartNew((Func<List<Treibladungspackung>>)takeTreibladungspackungenFromStorage);
                var effektladungsTask = Task.Factory.StartNew((Func<List<Effektladung>>)takeEffektLadungenFromStorage);

                Logger.Log("{0}: waiting for parts to become available", produzent.ID);
                Task.WaitAll(holzstabTask, gehauseTask, treibladungsTask, effektladungsTask);
                
                Logger.Log("{0}: all parts ready to assemble", produzent.ID);
                Helpers.SleepRandom(1000, 2000);

                rocket.Bestandteile.Add(holzstabTask.Result);
                rocket.Bestandteile.Add(gehauseTask.Result);
                rocket.Bestandteile.UnionWith(treibladungsTask.Result);
                rocket.Bestandteile.UnionWith(effektladungsTask.Result);

                Logger.Log("{0}: assembled rocket {1}, adding it to finishedRocketsStorage", produzent.ID, rocket.ID);
                finishedRocketsStorage.Put(transaction, rocket);

                XcoKernelSingleton.Instance.CommitTransaction(transaction);
            }
            catch (XcoSpaces.Exceptions.XcoContainerNotFoundException ex)
            {
                Logger.Log("exxxxx = " + ex);
                //this exception means the space (or a necessary container) can't be reached - usually this happens if the space is shutting down
                XcoKernelSingleton.Instance.RollbackTransaction(transaction);
            }
            catch (XcoSpaces.Exceptions.XcoTransactionException ex)
            {
                //Transaction timed out, return to Run:
                XcoKernelSingleton.Instance.RollbackTransaction(transaction);
                return;
            }
            catch (XcoSpaces.Exceptions.XcoException ex)
            {
                Logger.Log("exxxxx = " + ex);
                //ignore the error if the take-operation is interrupted because the System is shutting down
                if (ex.Message.Equals("Core shut down."))
                    return;
                else
                    throw;
            }
            catch (AggregateException ex)
            {
                if (ex.InnerException is XcoSpaces.Exceptions.XcoTransactionException)
                {
                    //Transaction timed out, return to Run:
                    XcoKernelSingleton.Instance.RollbackTransaction(transaction);
                    return;
                }
                ex.Handle(x => disposing);
            }
            catch (Exception ex)
            {
                Logger.Log("{0}: Unexpected error: {1}, shutting down! ", produzent.ID, ex);
                //ex.Handle(x => disposing);
                XcoKernelSingleton.Instance.RollbackTransaction(transaction);
                disposing = true;
                return;
            }
        }

        private Auftrag tryToGetAuftrag()
        {
            Auftrag auftrag = null;
            try
            {
                // find out, if there is a Auftrag to do
                auftrag = (Auftrag)AuftragsStorage.Take(transaction, CONST_MAX_TIME_TO_WAIT_FOR_AUFTRAG, new LabelSelector<string>("status", AuftragsStatus.Offen.ToString()));
                Logger.Log("{0}: Auftragstatus {1}: {2} rockets already produced", produzent.ID, auftrag.ID, auftrag.anzahlBereitsGebaut);
                if (auftrag.anzahlBereitsGebaut < auftrag.anzahl)
                {
                    auftrag.anzahlBereitsGebaut++;
                    AuftragsStorage.Put(transaction, auftrag, new FifoSelector(), new KeySelector<string>("id", auftrag.ID), new LabelSelector<string>("status", AuftragsStatus.Offen.ToString()));
                    //AuftragsStatusStorage.Put(transaction, auftragStatus, new KeySelector<string>("status", auftrag.ID));
                }
                else
                {
                    //TODO: Auftrag in Liste mit fertigen Aufträgen verschieben?
                    AuftragsStorage.Put(transaction, auftrag, new FifoSelector(), new KeySelector<string>("id", auftrag.ID), new LabelSelector<string>("status", AuftragsStatus.Offen.ToString()));
                    //AuftragsStatusStorage.Put(transaction, auftragStatus, new KeySelector<string>("status", auftrag.ID));
                    auftrag = null;
                }
            }
            catch (XcoSpaces.Exceptions.XcoOperationFailedException ex)
            {
                if (ex.Message.StartsWith("Operation timed out"))
                {
                    // if there is no Auftrag, we store null, so we just build a random rocket
                    auftrag = null;
                }
                else
                {
                    XcoKernelSingleton.Instance.RollbackTransaction(transaction);
                    throw ex;
                }
            }
            catch (XcoSpaces.Exceptions.XcoException ex)
            {
                XcoKernelSingleton.Instance.RollbackTransaction(transaction);
                Logger.Log("{0}: unexpected error: {1}", ex);
                throw ex;
            }
            if(auftrag != null)
                Logger.Log("{0}: found Auftrag {1} producing:", produzent.ID, auftrag.ID);
            else
                Logger.Log("{0}: no Auftrag found, producing random rocket!", produzent.ID);
            return auftrag;
        }

        private List<Treibladungspackung> takeTreibladungspackungenFromStorage()
        {
            var packungen = new List<Treibladungspackung>();
            var noetigeMenge = Random.Next(115, 145); //130g  +/-15g
            var menge = 0;

            while (menge < noetigeMenge)
            {
                var packung = takeTreibladungspackungFromStorage();
                var entnahme = packung.Take(noetigeMenge - menge);

                menge += entnahme.Treibladung;
                packungen.Add(entnahme);

                if (packung.Stand != PackungsStand.Leer)
                {
                    Logger.Log("{0}: Returning Treibladungspackung ({1}) to Available Parts", produzent.ID, packung.ToString());
                    treibladungspackungStorage.Put(transaction, packung, new LabelSelector<string>("Stand", packung.Stand.ToString()));
                }
            }

            return packungen;
        }

        private Treibladungspackung takeTreibladungspackungFromStorage()
        {
            Treibladungspackung packung;
            try
            {
                // prefer already used packages
                packung = (Treibladungspackung)treibladungspackungStorage.Take(transaction, CONST_MAX_TIME_TO_WAIT_FOR_USED_TREIBLADUNGSPACKUNG,
                                                                                                    new LabelSelector<string>("Stand", "Angebrochen"));
            }
            //there seems to be a bug in the XcoSpaces-Libaray, sometimes Timeout causes the error which is catched in the first block...
            catch (XcoSpaces.Exceptions.XcoOperationFailedException ex)
            {
                if (ex.Message.StartsWith("Operation timed out"))
                {
                    //if there is no open Treibladungspackung, then we have to take a new one:
                    packung = (Treibladungspackung)treibladungspackungStorage.Take(transaction);
                }
                else
                    throw ex;
            }
            Logger.Log("{0}: collected Treibladungspackung {1}", produzent.ID, packung.ID);
            return packung;
        }

        private List<Effektladung> takeEffektLadungenFromStorage()
        {
            var ladungen = new List<Effektladung>();

            int farbNr = 0;
            while (ladungen.Count < Rakete.CONST_EffektLadungenNeededPerRocket)
            {
                Effektladung effektladung;
                if (auftrag == null)
                    effektladung = (Effektladung)effektladungStorage.Take(transaction);
                else
                {
                    effektladung = (Effektladung)effektladungStorage.Take(transaction, Timeout.Infinite, 
                                                                                    new LabelSelector<string>("Farbe", auftrag.farben[farbNr].ToString()));
                    farbNr++;
                }
                Logger.Log("{0}: collected Effektladung {1}", produzent.ID, effektladung.ID);
                ladungen.Add(effektladung);
            }
            return ladungen;
        }

        private Holzstab takeHolzstabFromStorage()
        {
            var holzstab = (Holzstab)holzstabStorage.Take(transaction);
            Logger.Log("{0}: collected Holzstab {1}", produzent.ID, holzstab.ID);
            return holzstab;
        }

        private Gehaeuse takeGehaeuseFromStorage()
        {
            var gehaeuse = (Gehaeuse)gehaeuseStorage.Take(transaction);
            Logger.Log("{0}: collected Gehaeuse {1}", produzent.ID, gehaeuse.ID);
            return gehaeuse;
        }


        public override void Dispose()
        {
            if (!disposing)
            {
                disposing = true;

                Logger.Log("Disposing, stopping Worker {0}", produzent.ID);
                AuftragsStorage.Dispose();
                holzstabStorage.Dispose();
                gehaeuseStorage.Dispose();
                treibladungspackungStorage.Dispose();
                effektladungStorage.Dispose();
                finishedRocketsStorage.Dispose();

                base.Dispose();
            }
        }
    }
}
