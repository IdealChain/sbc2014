﻿using Feuerwerksfabrik.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using XcoSpaces.Exceptions;

namespace Feuerwerksfabrik.Space.Tasks
{
    class AuftraggeberTask : IDisposable
    {
        public AuftraggeberTask()
        {
            try
            {
                Logger.Log("Setting XcoKernel up for remote communication...");
                XcoKernelSingleton.Instance.Start(25555);
            }
            catch (XcoCommunicationException e)
            {
                Logger.Log("Ignoring: " + e.Message);
            }
        }

        public static AuftraggeberTask Create()
        {
            AuftraggeberTask at = new AuftraggeberTask();
            return at;
        }

        public void Run()
        {
            Console.ReadKey();
        }

        public virtual void Dispose()
        { }
    }
}
