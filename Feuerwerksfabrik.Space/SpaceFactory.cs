﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;
using Feuerwerksfabrik.Space.Tasks;

namespace Feuerwerksfabrik.Space
{
    [Export(typeof(IFeuerwerksfabrikFactory))]
    public class SpaceFactory : IFeuerwerksfabrikFactory
    {
        public string Name
        {
            get { return "Space"; }
        }

        public IFeuerwerksfabrikEngine Create()
        {
            return new SpaceEngine();
        }

        public Task CreateMitarbeiterTask(Mitarbeiter mitarbeiter)
        {
            if (mitarbeiter is Produzent)
            {
                ProduzentWorker pw = ProduzentWorker.Create((Produzent)mitarbeiter);
                return new Task(pw.Run);
            }
            else if (mitarbeiter is QA)
            {
                QaWorker qaWorker = QaWorker.Create((QA)mitarbeiter);
                return new Task(qaWorker.Run);
            }
            else if (mitarbeiter is Logistiker)
            {
                LogistikerWorker lw = LogistikerWorker.Create((Logistiker)mitarbeiter);
                return new Task(lw.Run);
            }

            throw new NotSupportedException("Mitarbeiter type not supported: " + mitarbeiter.GetType().Name);
        }
    }
}
