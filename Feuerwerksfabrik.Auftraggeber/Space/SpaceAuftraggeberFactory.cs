﻿using System;
using Feuerwerksfabrik.Common;

namespace Feuerwerksfabrik.Auftraggeber.Space
{
    public class SpaceAuftraggeberFactory : IAuftraggeberFactory
    {
        public string Name
        {
            get { return "Space"; }
        }

        public IAuftraggeberEngine Create(string name)
        {
            return new SpaceAuftraggeberEngine(name);
        }

        public string DefaultDeliverySpace
        {
            get { return "localhost:25555"; }
        }

        public string DefaultDeliveryStorage
        {
            get { return "deliverHere"; }
        }
    }
}
