﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Feuerwerksfabrik.Common.Objects;
using Feuerwerksfabrik.Space;
using Feuerwerksfabrik.Space.Storage;
using Feuerwerksfabrik.Space.Properties;
using XcoSpaces.Kernel.Selectors;
using XcoSpaces.Kernel;
using System.Diagnostics;
using XcoSpaces.Exceptions;
using Feuerwerksfabrik.Common;
using System.Threading;
using Feuerwerksfabrik.Common.Interfaces;

namespace Feuerwerksfabrik.Auftraggeber.Space
{
    internal class SpaceAuftraggeberEngine : IAuftraggeberEngine
    {
        private SpaceStorage<Auftrag> AuftragsStorage;
        private SpaceStorage<Rakete> DeliverStorage;
        private SpaceStorage<Rakete> AuftraggeberStorage;

        public ObservableCollection<Rakete> DeliveredRockets { get; private set; }
        private readonly string NAME;

        public SpaceAuftraggeberEngine(string name)
        {
            NAME = name;
            DeliveredRockets = new ObservableCollection<Rakete>();

            Logger.Log("Starting XcoKernel on port 25555");
            XcoKernelSingleton.Instance.Start(25555);
            AuftragsStorage = SpaceStorage<Auftrag>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, Settings.Default.ContainerName_AuftragsStorage);
            //string depotName = "depot_" + name;
            //AuftraggeberStorage = SpaceStorage<Rakete>.Create(depotName);
            //Logger.Log("depot = " + depotName);
            DeliverStorage = SpaceStorage<Rakete>.Create("deliverHere", new FifoSelector());
            registerDeliveryListener();

            var rockets = checkForReadyDeliverys();
            lock (DeliveredRockets)
            {
                foreach (var rocket in rockets)
                {
                    Logger.Log("Rocket in depot: " + rocket.ID);
                    DeliveredRockets.Add(rocket);
                }
            }
        }

        public List<Rakete> checkForReadyDeliverys()
        {
            List<Rakete> raketen = new List<Rakete>();
            SpaceStorage<Rakete> RaketenStorage;
            try
            {
                RaketenStorage = SpaceStorage<Rakete>.Get(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, "depot_" + NAME);
            }
            catch (XcoSpaces.Exceptions.XcoContainerNotFoundException)
            {
                //nothing waiting in depot
                return raketen;
            }
            TransactionReference transaction = XcoKernelSingleton.Instance.CreateTransaction(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, 10000);

            bool loop = true;
            while (loop)
            {
                try
                {
                    Rakete rakete = RaketenStorage.Take(transaction, 500, new FifoSelector());
                    raketen.Add(rakete);
                }
                catch (XcoException ex)
                {
                    Logger.Log("ex = " + ex);
                    loop = false;
                }
            }
            XcoKernelSingleton.Instance.CommitTransaction(transaction);
            return raketen;
        }

        public void registerDeliveryListener()
        {
            Logger.Log("Registering delivery listener");
            DeliverStorage.setPutListener((source, operation, entries) =>
            {
                foreach (var entry in entries)
                {
                    var rakete = (Rakete)entry.Value;
                    Logger.Log("Rocket delivered: {0}", rakete.ID);

                    lock (DeliveredRockets)
                        DeliveredRockets.Add(rakete);
                }
            });
        }

        public void unregisterDeliveryListener()
        {
            DeliverStorage.clearPutListner();
        }

        public void SubmitOrder(Auftrag auftrag)
        {
            TransactionReference transaction = XcoKernelSingleton.Instance.CreateTransaction(Settings.Default.SpaceHost + ":" + Settings.Default.SpacePort, Timeout.Infinite);
            AuftragsStorage.Put(transaction, auftrag, new FifoSelector(), new KeySelector<string>("id", auftrag.ID.ToString()), new LabelSelector<string>("status", AuftragsStatus.Offen.ToString()));
            XcoKernelSingleton.Instance.CommitTransaction(transaction);
        }

        public void Dispose()
        {
            unregisterDeliveryListener();
            XcoKernelSingleton.Dispose();
        }
    }
}
