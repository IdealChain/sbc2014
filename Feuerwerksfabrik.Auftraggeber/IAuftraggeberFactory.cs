﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using Feuerwerksfabrik.Common;

namespace Feuerwerksfabrik.Auftraggeber
{
    public interface IAuftraggeberFactory
    {
        String Name { get; }
        IAuftraggeberEngine Create(string name);
        String DefaultDeliverySpace { get; }
        String DefaultDeliveryStorage { get; }
    }
}
