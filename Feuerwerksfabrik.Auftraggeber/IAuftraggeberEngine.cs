﻿using System.Collections.ObjectModel;
using Feuerwerksfabrik.Common.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Feuerwerksfabrik.Auftraggeber
{
    public interface IAuftraggeberEngine : IDisposable
    {
        ObservableCollection<Rakete> DeliveredRockets { get; }
        void SubmitOrder(Auftrag order);
    }
}
