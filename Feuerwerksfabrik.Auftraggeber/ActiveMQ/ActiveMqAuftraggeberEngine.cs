﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Apache.NMS;
using Apache.NMS.ActiveMQ;
using Feuerwerksfabrik.ActiveMQ;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Objects;
using System.Diagnostics;

namespace Feuerwerksfabrik.Auftraggeber.ActiveMQ
{
    internal class ActiveMqAuftraggeberEngine : IAuftraggeberEngine, IDisposable
    {
        public static readonly string CONST_ADDRESS = "tcp://localhost:61616/";
        public static readonly string CONST_DELIVERYSTORAGE = "placeHere";

        public static readonly string CONST_REMOTE_ADDRESS = "tcp://localhost:61616/";

        private ConnectionFactory factory;
        private IConnection conn;
        private ISession session;
        private IQueue deliveryStorage;
        private IMessageConsumer deliveryStorageConsumer;

        private ConnectionFactory feuerwerksFabrikFactory;
        private IConnection feuerwerksFabrikConn;
        private ISession feuerwerksFabrikSession;
        private IQueue feuerwerksFabrikDeliveryDepot;
        private IMessageConsumer feuerwerksFabrikDeliveryDepotConsumer;
        private IQueue auftraegeStorage;
        private IMessageProducer auftraegeStorageProducer;
        private EventLogger eventLogger;

        private readonly string NAME;

        public ActiveMqAuftraggeberEngine(string name)
        {
            NAME = name;
            DeliveredRockets = new ObservableCollection<Rakete>();

            factory = new ConnectionFactory(CONST_ADDRESS);
            conn = factory.CreateConnection();
            conn.Start();
            session = conn.CreateSession(AcknowledgementMode.Transactional);
            deliveryStorage = session.GetQueue(CONST_DELIVERYSTORAGE);
            deliveryStorageConsumer = session.CreateConsumer(deliveryStorage);

            feuerwerksFabrikFactory = new ConnectionFactory(CONST_REMOTE_ADDRESS);
            feuerwerksFabrikConn = factory.CreateConnection();
            feuerwerksFabrikConn.Start();
            feuerwerksFabrikSession = conn.CreateSession(AcknowledgementMode.Transactional);
            auftraegeStorage = feuerwerksFabrikSession.GetQueue(ActiveMqEngine.CONST_AUFTRAEGE_STORAGE);
            auftraegeStorageProducer = feuerwerksFabrikSession.CreateProducer(auftraegeStorage);
            feuerwerksFabrikDeliveryDepot = feuerwerksFabrikSession.GetQueue("depot_" + NAME);
            feuerwerksFabrikDeliveryDepotConsumer = feuerwerksFabrikSession.CreateConsumer(feuerwerksFabrikDeliveryDepot);

            eventLogger = new EventLogger(feuerwerksFabrikSession);
            registerDeliveryListener();

            var rockets = checkForReadyDeliverys();
            lock (DeliveredRockets)
            {
                foreach (var rocket in rockets)
                {
                    Logger.Log("Rocket in depot: " + rocket.ID);
                    DeliveredRockets.Add(rocket);
                }
            }
        }

        public void SubmitOrder(Auftrag auftrag)
        {
            eventLogger.sendEventLog(EventLogType.ADD, auftrag, ActiveMqEngine.CONST_AUFTRAEGE_STORAGE);
            IObjectMessage msg = feuerwerksFabrikSession.CreateObjectMessage(auftrag);
            auftraegeStorageProducer.Send(msg);
            feuerwerksFabrikSession.Commit();
        }

        public ObservableCollection<Rakete> DeliveredRockets { get; private set; }

        public List<Common.Objects.Rakete> checkForReadyDeliverys()
        {
            List<Common.Objects.Rakete> raketen = new List<Common.Objects.Rakete>();
            while (true)
            {
                IObjectMessage iom = (IObjectMessage)feuerwerksFabrikDeliveryDepotConsumer.Receive(new TimeSpan(0, 0, 3));
                if (iom == null)
                    break;
                Rakete rakete = (Rakete)iom.Body;
                raketen.Add(rakete);
            }
            return raketen;
        }

        public void registerDeliveryListener()
        {
            Logger.Log("Registering delivery listener");
            deliveryStorageConsumer.Listener += onMessage;
        }

        public void unregisterDeliveryListener()
        {
            deliveryStorageConsumer.Listener -= onMessage;
        }

        private void onMessage(IMessage message)
        {
            IObjectMessage iom = (IObjectMessage)message;
            Rakete rakete = (Rakete)iom.Body;
            Logger.Log("Rocket delivered: {0}", rakete.ID);

            lock (DeliveredRockets)
                DeliveredRockets.Add(rakete);
        }

        public void Dispose()
        {
            unregisterDeliveryListener();
            session.Commit();
            //clean up remote connection:
            session.Close();
            conn.Close();
            conn.Dispose();
            conn = null;
            factory = null;
        }
    }
}
