﻿using Feuerwerksfabrik.Common;

namespace Feuerwerksfabrik.Auftraggeber.ActiveMQ
{
    public class ActiveMqAuftraggeberFactory : IAuftraggeberFactory
    {
        public string Name
        {
            get { return "ActiveMQ"; }
        }

        public IAuftraggeberEngine Create(string name)
        {
            return new ActiveMqAuftraggeberEngine(name);
        }

        public string DefaultDeliverySpace
        {
            get { return ActiveMqAuftraggeberEngine.CONST_ADDRESS; }
        }

        public string DefaultDeliveryStorage
        {
            get { return ActiveMqAuftraggeberEngine.CONST_DELIVERYSTORAGE; }
        }
    }
}
