﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition.Hosting;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.GUI.Helpers;
using Feuerwerksfabrik.GUI.ViewModels;

namespace Feuerwerksfabrik.GUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            CompositionInitializer.AddCatalog(new AssemblyCatalog(Assembly.GetExecutingAssembly()));
            InitializeComponent();
        }

        public ViewModel Model
        {
            get { return DataContext as ViewModel; }
            set { DataContext = value; }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            if (Model != null)
            {
                Model.Engine.CreateEngine();
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            CompositionInitializer.Dispose();
        }

    }
}
