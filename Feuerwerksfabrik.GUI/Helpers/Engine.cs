using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Windows.Data;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;
using Feuerwerksfabrik.Common.Objects;
using Feuerwerksfabrik.GUI.Annotations;
using Feuerwerksfabrik.GUI.Properties;

namespace Feuerwerksfabrik.GUI.Helpers
{
    /// <summary>
    /// Single-instance fireworks engine management class.
    /// </summary>
    [Export(typeof(Engine))]
    [PartCreationPolicy(CreationPolicy.Shared)]
    public class Engine : IDisposable, INotifyPropertyChanged
    {
        private IFeuerwerksfabrikEngine activeEngine;
        private MitarbeiterTasks mitarbeiterTasks;

        /// <summary>
        /// Enumerates the currently available engine factories.
        /// Automatically injected via MEF.
        /// </summary>
        [ImportMany]
        public IEnumerable<IFeuerwerksfabrikFactory> EngineFactories { get; private set; }

        /// <summary>
        /// Currently selected engine factory.
        /// </summary>
        public IFeuerwerksfabrikFactory SelectedEngineFactory
        {
            get { return EngineFactories.FirstOrDefault(e => e.Name.Equals(Settings.Default.SelectedEngineFactory)); }
            set
            {
                Settings.Default.SelectedEngineFactory = value != null ? value.Name : String.Empty;
                Settings.Default.Save();

                OnPropertyChanged();
                CreateEngine();
            }
        }

        /// <summary>
        /// Instantiate fireworks engine based on the currently selected factory.
        /// </summary>
        public void CreateEngine()
        {
            ClearEngine();
            if (SelectedEngineFactory == null)
                return;

            try
            {
                var engine = SelectedEngineFactory.Create();

                // enable automatic UI thread sync. and dispatcher invocation
                BindingOperations.EnableCollectionSynchronization(engine.Auftraege, engine.Auftraege);
                BindingOperations.EnableCollectionSynchronization(engine.AvailableParts, engine.AvailableParts);
                BindingOperations.EnableCollectionSynchronization(engine.DefectRockets, engine.DefectRockets);
                BindingOperations.EnableCollectionSynchronization(engine.DeliveredRocketPackages,
                    engine.DeliveredRocketPackages);
                BindingOperations.EnableCollectionSynchronization(engine.FinishedRockets, engine.FinishedRockets);
                BindingOperations.EnableCollectionSynchronization(engine.VerifiedRockets, engine.VerifiedRockets);

                ActiveEngine = engine;
            }
            catch (Exception e)
            {
                Logger.Log("Engine creation: {0}", e.Message);
                SelectedEngineFactory = null;
            }
        }

        /// <summary>
        /// Currently active fireworks engine.
        /// </summary>
        public IFeuerwerksfabrikEngine ActiveEngine
        {
            get { return activeEngine; }
            private set
            {
                activeEngine = value;
                OnPropertyChanged();

                MitarbeiterTasks = new MitarbeiterTasks(SelectedEngineFactory, activeEngine);
            }
        }

        /// <summary>
        /// Gets the list of currently active worker tasks.
        /// </summary>
        public MitarbeiterTasks MitarbeiterTasks
        {
            get { return mitarbeiterTasks; }
            private set
            {
                mitarbeiterTasks = value;
                OnPropertyChanged();
            }
        }

        private void ClearEngine()
        {
            if (MitarbeiterTasks != null)
            {
                MitarbeiterTasks.Dispose();
                MitarbeiterTasks = null;
            }

            if (ActiveEngine != null)
            {
                ActiveEngine.Dispose();
                ActiveEngine = null;
            }
        }

        /// <summary>
        /// Current number of produced rockets: defect and working
        /// </summary>
        public int ProducedRockets
        {
            get
            {
                var count = 0;
                count += ActiveEngine.DefectRockets.Count;
                count += ActiveEngine.VerifiedRockets.Count;
                count += activeEngine.DeliveredRocketPackages.Select(p => p.Raketen.Count).Sum();
                return count;
            }
        }

        private bool isBenchmarkRunning;

        public bool IsBenchmarkRunning
        {
            get { return isBenchmarkRunning; }
            set
            {
                if (value == isBenchmarkRunning)
                    return;

                isBenchmarkRunning = value;
                Common.Helpers.SleepRandomDisabled = value;
                OnPropertyChanged();
            }
        }

        public void Benchmark()
        {
            try
            {
                MitarbeiterTasks.StopAll();
                IsBenchmarkRunning = true;

                if (ProducedRockets != 0)
                {
                    Logger.Log("Output storages not empty!");
                    return;
                }

                Logger.Log("Preparing resources...");
                MitarbeiterTasks.Start(new Lieferant(typeof(Holzstab), 1500));
                MitarbeiterTasks.Start(new Lieferant(typeof(Gehaeuse), 1500));
                MitarbeiterTasks.Start(new Lieferant(typeof(Effektladung), 1500, 0.05, Effektfarbe.Rot));
                MitarbeiterTasks.Start(new Lieferant(typeof(Effektladung), 1500, 0.05, Effektfarbe.Gruen));
                MitarbeiterTasks.Start(new Lieferant(typeof(Effektladung), 1500, 0.05, Effektfarbe.Blau));
                MitarbeiterTasks.Start(new Lieferant(typeof(Treibladungspackung), 500));
                MitarbeiterTasks.WaitOnAll();

                Logger.Log("Starting workers...");
                MitarbeiterTasks.Start(new Produzent { ID = "bench-prod-1" });
                MitarbeiterTasks.Start(new Produzent { ID = "bench-prod-2" });
                MitarbeiterTasks.Start(new QA { ID = "bench-qa" });
                MitarbeiterTasks.Start(new Logistiker { ID = "bench-logistiker" });

                Logger.Log("Benchmarking for 60 seconds...");
                Thread.Sleep(60000);

                Logger.Log("Stopping workers...");
                MitarbeiterTasks.StopAll();
                MitarbeiterTasks.WaitOnAll();

                Logger.Log("Produced rockets: {0}/min, {1:0.0}/sec.", ProducedRockets, ProducedRockets / 60.0);
            }
            finally
            {
                IsBenchmarkRunning = false;
            }
        }

        #region IDisposable

        public void Dispose()
        {
            ClearEngine();
        }

        #endregion

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        public virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}