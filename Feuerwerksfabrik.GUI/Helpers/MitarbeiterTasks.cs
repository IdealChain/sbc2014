﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Data;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Interfaces;
using Feuerwerksfabrik.GUI.Properties;
using Feuerwerksfabrik.Mitarbeiter;

namespace Feuerwerksfabrik.GUI.Helpers
{
    /// <summary>
    /// ObservableCollection managing and containing the currently running MitarbeiterTasks.
    /// </summary>
    public class MitarbeiterTasks : ObservableCollection<MitarbeiterTask>, IDisposable
    {
        private IFeuerwerksfabrikFactory Factory { get; set; }
        private IFeuerwerksfabrikEngine Engine { get; set; }

        public MitarbeiterTasks(IFeuerwerksfabrikFactory factory, IFeuerwerksfabrikEngine engine)
        {
            Factory = factory;
            Engine = engine;

            // enable automatic UI thread sync. and dispatcher invocation
            BindingOperations.EnableCollectionSynchronization(this, this);
        }

        private static bool MitarbeiterThreadsEnabled
        {
            get { return Settings.Default.MitarbeiterThreads; }
        }

        public void Start<TMitarbeiter>(TMitarbeiter mitarbeiter) where TMitarbeiter : Common.Actors.Mitarbeiter
        {
            try
            {
                MitarbeiterTask task;
                var lieferant = mitarbeiter as Lieferant;

                if (lieferant != null)
                {
                    var cancellation = new CancellationTokenSource();
                    var token = cancellation.Token;
                    task = new MitarbeiterTask(lieferant.ID, Engine.CreateLieferantTask(lieferant, token), cancellation, TaskFinished);
                }
                else if (MitarbeiterThreadsEnabled)
                {
                    task = new MitarbeiterTask(mitarbeiter.ID, Factory.CreateMitarbeiterTask(mitarbeiter), finished: TaskFinished);
                }
                else
                {
                    var exe = MitarbeiterConsole.GetExecutableName();
                    var args = MitarbeiterConsole.ComposeArguments(
                        Factory,
                        typeof(TMitarbeiter),
                        mitarbeiter.ID);

                    var psi = new ProcessStartInfo(exe, args);
                    task = new MitarbeiterTask(mitarbeiter.ID, psi, TaskFinished);
                }
                Add(task);
            }
            catch (Exception e)
            {
                Logger.Log("Task creation: {0}", e.Message);
            }
        }

        private void TaskFinished(MitarbeiterTask task)
        {
            Logger.Log("Finished: {0}", task.ToString());
            lock (this)
                Remove(task);
        }

        /// <summary>
        /// Stop all running tasks
        /// </summary>
        public void StopAll()
        {
            foreach (var task in this)
                task.Stop();

            Clear();
        }

        /// <summary>
        /// Wait until all running tasks are finished
        /// </summary>
        public void WaitOnAll()
        {
            while (Count > 0)
                Thread.Sleep(500);
        }

        public void Dispose()
        {
            StopAll();
        }
    }
}
