using System;
using System.Diagnostics;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Input;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;

namespace Feuerwerksfabrik.GUI.Helpers
{
    /// <summary>
    /// A Mitarbeiter worker that is currently running, either as a Thread (Task) or a Process.
    /// </summary>
    public class MitarbeiterTask
    {
        public ID ID { get; private set; }

        public TaskType Type
        {
            get { return task != null ? TaskType.Task : TaskType.Prozess; }
        }

        public enum TaskType
        {
            Task, Prozess
        }

        private readonly Task task;
        private readonly CancellationTokenSource taskCancellationTokenSource;
        private readonly Process process;
        private readonly Action<MitarbeiterTask> finished;

        public MitarbeiterTask(ID id, Task task, CancellationTokenSource cancellation = null, Action<MitarbeiterTask> finished = null)
        {
            this.ID = id;
            this.task = task;
            this.taskCancellationTokenSource = cancellation;
            this.finished = finished;

            Logger.Log("Starting task: {0}", ID);
            task.ContinueWith(OnFinished);
            task.Start();
        }

        public MitarbeiterTask(ID id, ProcessStartInfo psi, Action<MitarbeiterTask> finished = null)
        {
            this.ID = id;
            this.finished = finished;

            Logger.Log("Starting process: {0} ({1})", ID, Path.GetFileName(psi.FileName));
            process = Process.Start(psi);
            if (process != null)
            {
                process.EnableRaisingEvents = true;
                process.Exited += OnProcessFinished;
            }
        }

        private void OnFinished(Task t)
        {
            if (finished != null)
                finished(this);
        }

        private void OnProcessFinished(object sender, EventArgs e)
        {
            if (process != null)
                process.Exited -= OnProcessFinished;
            if (finished != null)
                finished(this);
        }

        public void Stop()
        {
            try
            {
                if (task != null && taskCancellationTokenSource != null)
                    taskCancellationTokenSource.Cancel();
                if (process != null)
                    process.Kill();
            }
            catch (Exception e)
            {
                Logger.Log("Unable to kill {0}: {1}", ID, e.Message);
            }
        }

        private RelayCommand stopCommand;

        public ICommand StopCommand
        {
            get { return stopCommand ?? (stopCommand = new RelayCommand(o => Stop())); }
        }

        public override string ToString()
        {
            return String.Format("{0} [{1}]", ID, Type.ToString());
        }
    }
}