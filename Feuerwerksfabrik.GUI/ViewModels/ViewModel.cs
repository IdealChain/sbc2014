﻿using System;
using System.Collections.Generic;
using System.ComponentModel.Composition;
using System.Diagnostics;
using System.Reflection;
using System.Windows;
using Feuerwerksfabrik.GUI.Helpers;
using Feuerwerksfabrik.GUI.Properties;
using System.Windows.Input;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using System.Threading.Tasks;
using Feuerwerksfabrik.Mitarbeiter;

namespace Feuerwerksfabrik.GUI.ViewModels
{
    public class ViewModel
    {
        public ViewModel()
        {
            CompositionInitializer.SatisfyImports(this);
        }

        public String Title
        {
            get
            {
                var assembly = Assembly.GetExecutingAssembly().GetName();
                return string.Format("{0} {1}", assembly.Name, assembly.Version);
            }
        }

        public WindowState State
        {
            get { return Settings.Default.WindowState; }
            set
            {
                Settings.Default.WindowState = value;
                Settings.Default.Save();
            }
        }

        [Import]
        public Engine Engine { get; private set; }

        public ICommand StartProduzentCommand { get { return GetStartCommand<Produzent>(); } }
        public ICommand StartQACommand { get { return GetStartCommand<QA>(); } }
        public ICommand StartLogistikerCommand { get { return GetStartCommand<Logistiker>(); } }

        private readonly Dictionary<Type, ICommand> startCommands = new Dictionary<Type, ICommand>();

        private ICommand GetStartCommand<TMitarbeiter>() where TMitarbeiter : Common.Actors.Mitarbeiter, new()
        {
            if (!startCommands.ContainsKey(typeof(TMitarbeiter)))
                startCommands[typeof(TMitarbeiter)] = new RelayCommand(p => StartMitarbeiter<TMitarbeiter>(), p => Engine.MitarbeiterTasks != null);

            return startCommands[typeof(TMitarbeiter)];
        }

        private void StartMitarbeiter<TMitarbeiter>() where TMitarbeiter : Common.Actors.Mitarbeiter, new()
        {
            if (Engine.MitarbeiterTasks == null)
                return;

            var mitarbeiter = new TMitarbeiter { ID = ID.GenerateID<TMitarbeiter>() };
            Engine.MitarbeiterTasks.Start(mitarbeiter);
        }

        private ICommand benchmarkCommand;

        public ICommand StartBenchmarkCommand
        {
            get { return benchmarkCommand ?? (benchmarkCommand = new RelayCommand(p => StartBenchmark())); }
        }

        private void StartBenchmark()
        {
            var benchmark = new Task(Engine.Benchmark);
            benchmark.Start();
        }
    }
}
