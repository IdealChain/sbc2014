﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Objects;
using Feuerwerksfabrik.GUI.Annotations;
using Feuerwerksfabrik.GUI.Helpers;

namespace Feuerwerksfabrik.GUI.ViewModels
{
    public class SelectedPartModel : INotifyPropertyChanged
    {
        private Type selectedPart;
        private int count;

        public SelectedPartModel()
        {
            CompositionInitializer.SatisfyImports(this);

            SelectedPart = AvailableParts.FirstOrDefault();
            Count = 100;
            FailureRate = 0.2;
            SelectedColor = Effektfarbe.Rot;
        }

        public IEnumerable<Type> AvailableParts
        {
            get
            {
                return new List<Type>
                {
                    typeof(Holzstab),
                    typeof(Gehaeuse),
                    typeof(Treibladungspackung),
                    typeof(Effektladung)
                };
            }
        }

        public Type SelectedPart
        {
            get { return selectedPart; }
            set
            {
                if (value == selectedPart) return;
                selectedPart = value;
                OnPropertyChanged();
                OnPropertyChanged("IsEffektLadung");
                OnPropertyChanged("IsValid");
            }
        }

        public bool IsEffektLadung
        {
            get { return SelectedPart != null && SelectedPart == typeof(Effektladung); }
        }

        public bool IsValid
        {
            get { return SelectedPart != null && Count > 0; }
        }

        public int Count
        {
            get { return count; }
            set
            {
                if (value == count) return;
                count = value;
                OnPropertyChanged();
                OnPropertyChanged("IsValid");
            }
        }

        public double FailureRate { get; set; }

        public IEnumerable<Effektfarbe> AvailableColors
        {
            get { return Enum.GetValues(typeof(Effektfarbe)).Cast<Effektfarbe>(); }
        }

        public Effektfarbe? SelectedColor { get; set; }

        private ICommand deliverCommand;

        public ICommand DeliverCommand
        {
            get { return deliverCommand ?? (deliverCommand = new RelayCommand(p => DeliverPart(), p => IsValid)); }
        }

        private ICommand deliverAllCommand;

        public ICommand DeliverAllCommand
        {
            get { return deliverAllCommand ?? (deliverAllCommand = new RelayCommand(p => DeliverAllParts(), p => IsValid)); }
        }

        [Import]
        public Engine Engine { get; private set; }

        private void DeliverPart()
        {
            if (Engine.MitarbeiterTasks == null)
                return;

            var lieferant = new Lieferant(SelectedPart, Count, FailureRate, SelectedColor);
            Engine.MitarbeiterTasks.Start(lieferant);
        }

        private void DeliverAllParts()
        {
            if (Engine.MitarbeiterTasks == null)
                return;

            foreach (var part in AvailableParts.Except(new[] { typeof(Effektladung) }))
            {
                var lieferant = new Lieferant(part, Count);
                Engine.MitarbeiterTasks.Start(lieferant);
            }

            // deliver Effektladungen
            foreach (var effectColor in AvailableColors)
            {
                var lieferant = new Lieferant(typeof(Effektladung), Count, FailureRate, effectColor);
                Engine.MitarbeiterTasks.Start(lieferant);
                
            }
        }

        #region INotifyPropertyChanged

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null) handler(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
