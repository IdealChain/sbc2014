﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Objects;

namespace Feuerwerksfabrik.GUI.UserControls
{
    /// <summary>
    /// Interaction logic for RocketPackageControl.xaml
    /// </summary>
    public partial class RocketPackageControl : UserControl
    {
        public RocketPackageControl()
        {
            InitializeComponent();
        }
    }

    public class DesignTimeRaketenPackung : Raketenpackung
    {
        public DesignTimeRaketenPackung()
        {
            var logistiker = new Logistiker { ID = Common.ID.GenerateID<Logistiker>() };

            var rakete1 = new DesignTimeRakete();
            rakete1.Mitarbeiter.Add(logistiker);

            var rakete2 = new DesignTimeRakete();
            rakete2.Mitarbeiter.Add(logistiker);

            ID = Common.ID.GenerateID<Raketenpackung>();
            Raketen.Add(rakete1);
            Raketen.Add(rakete2);

            Qualitaet = Raketenqualitaet.KlasseB;
        }
    }
}
