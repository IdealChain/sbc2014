﻿using System.Windows.Controls;
using Feuerwerksfabrik.GUI.ViewModels;

namespace Feuerwerksfabrik.GUI.UserControls
{
    /// <summary>
    /// Interaction logic for SelectedPartControl.xaml
    /// </summary>
    public partial class SelectedPartControl : UserControl
    {
        public SelectedPartControl()
        {
            InitializeComponent();
        }

        public SelectedPartModel Model
        {
            get { return DataContext as SelectedPartModel; }
            set { DataContext = value; }
        }

    }
}
