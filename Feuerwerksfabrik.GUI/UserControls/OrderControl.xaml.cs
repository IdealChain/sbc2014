﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Objects;

namespace Feuerwerksfabrik.GUI.UserControls
{
    /// <summary>
    /// Interaction logic for OrderControl.xaml
    /// </summary>
    public partial class OrderControl : UserControl
    {
        public OrderControl()
        {
            InitializeComponent();
        }
    }

    public class DesignTimeOrder : Auftrag
    {
        public DesignTimeOrder()
        {
            ID = ID.GenerateID<Auftrag>();
            auftraggeber = "auftraggeber-0000";
            deliverAddress = "localhost";
            deliverStorage = "here";
            anzahl = 10;
            anzahlBereitsGebaut = 2;
            anzahlBereitsUeberprueft = 3;
            farben = new List<Effektfarbe>{Effektfarbe.Rot, Effektfarbe.Gruen, Effektfarbe.Blau};
        }
    }
}
