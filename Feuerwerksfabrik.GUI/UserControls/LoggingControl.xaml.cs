﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Interop;
using System.Windows.Threading;
using System.Xaml;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.GUI.Helpers;

namespace Feuerwerksfabrik.GUI.UserControls
{
    /// <summary>
    /// Interaction logic for LoggingControl.xaml
    /// </summary>
    public partial class LoggingControl : UserControl
    {
        private Task workerTask;
        private readonly AutoResetEvent messagePending = new AutoResetEvent(false);
        private readonly ConcurrentQueue<String> messageQueue = new ConcurrentQueue<string>();

        public LoggingControl()
        {
            InitializeComponent();

            Logger.LogAction += Log;
            workerTask = Task.Factory.StartNew(UpdateLogDocumentTask);

            if (DesignerProperties.GetIsInDesignMode(this))
                Log("Logging Control");
        }

        private void Log(String msg)
        {
            messageQueue.Enqueue(msg);
            messagePending.Set();
        }

        private void UpdateLogDocumentTask()
        {
            var msges = new List<string>();

            while (true)
            {
                messagePending.WaitOne();

                bool gotMsg;
                do
                {
                    String msg;
                    gotMsg = messageQueue.TryDequeue(out msg);
                    if (gotMsg) msges.Add(msg);
                } while (gotMsg);

                var paragraphs = msges.Select(m => new Paragraph(new Run(m)));

                Dispatcher.Invoke(() =>
                {
                    LoggingFlow.Document.Blocks.AddRange(paragraphs);
                    Dispatcher.BeginInvoke(ScrollToEnd, DispatcherPriority.Normal, LoggingFlow);

                });

                msges.Clear();
                Thread.Sleep(50);
            }
        }

        private static readonly Action<LogScrollViewer> ScrollToEnd = (s =>
        {
            if (s != null && s.ScrollViewer != null)
                s.ScrollViewer.ScrollToEnd();
        });
    }
}
