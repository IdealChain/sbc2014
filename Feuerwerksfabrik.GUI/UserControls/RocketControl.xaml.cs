﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Feuerwerksfabrik.Common;
using Feuerwerksfabrik.Common.Actors;
using Feuerwerksfabrik.Common.Objects;

namespace Feuerwerksfabrik.GUI.UserControls
{
    /// <summary>
    /// Interaction logic for RocketControl.xaml
    /// </summary>
    public partial class RocketControl : UserControl
    {
        public RocketControl()
        {
            InitializeComponent();
        }
    }

    public class DesignTimeRakete : Rakete
    {
        public DesignTimeRakete()
        {
            ID = ID.GenerateID<Rakete>();
            Mitarbeiter.Add(new Produzent { ID = ID.GenerateID<Produzent>() });
            Auftrag = Auftrag.Create("auftraggeber-0000", "home", "home", 3, Effektfarbe.Rot, Effektfarbe.Gruen, Effektfarbe.Blau);
            Bestandteile.Add(Bestandteil.Create(new Lieferant(typeof(Gehaeuse), 1)));
            Bestandteile.Add(Bestandteil.Create(new Lieferant(typeof(Holzstab), 1)));
            Bestandteile.Add(Bestandteil.Create(new Lieferant(typeof(Treibladungspackung), 1)));
            Bestandteile.Add(Bestandteil.Create(new Lieferant(typeof(Effektladung), 1)));
        }
    }
}
