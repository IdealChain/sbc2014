sbc2014
=======

Verteiltes Programmieren mit Space Based Computing Middleware VU

*WS2014, TU Wien*

## Requirements

* Windows 7+
* Visual Studio 2013
* [Apache ActiveMQ 5.10.0](http://activemq.apache.org/activemq-5100-release.html)

## Building & Running

* Open the **Feuerwerksfabrik.sln** solution file in Visual Studio
* Run [**ActiveMQ**](http://activemq.apache.org/activemq-5100-release.html) by executing the activemq.bat in the folder bin with the argument "start":

		bin\activemq.bat start

	*Warning: ActiveMq keeps the messages persistent! Please **purge** the **data**-directory of ActiveMq between usage, to **prevent unforeseen** results!*

* Set **Feuerwerksfabrik.GUI** as active Startup Project
* Start Run / Debug

#### Attribution

Apache ActiveMQ by [The Apache Software Foundation](http://activemq.apache.org/) under [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0.html)  
Rocket Icon by [Steve Lianardo](http://www.iconarchive.com/artist/stevelianardo.html) licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)  
Box Icon by [Double-J Design](http://www.iconarchive.com/artist/double-j-design.html) licensed under [CC BY 4.0](https://creativecommons.org/licenses/by/4.0/)